package com.example.gendati_mobileapp;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    View background;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        background = findViewById(R.id.background_main_activity);

    }

    public void changerFond(View view) {
        background.setBackgroundColor(Color.rgb(generateRGBParameter(), generateRGBParameter(), generateRGBParameter()));
    }

    private int generateRGBParameter() {
        return new Random().nextInt(256);
    }
}
