﻿using System.Collections;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using gendati_backend.DAO;
using gendati_backend.DTO;
using gendati_backend.Utility;
using Microsoft.AspNetCore.Mvc;

namespace gendati_backend.Controllers
{
    [ApiController]
    [Route("/api/evenement")]
    public class EvenementController : ControllerBase
    {
        [HttpGet]
        public List<EvenementDTO> Query()
        {
            List<EvenementDTO> list = EvenementDAO.Query();
            list = list.OrderBy(x => x.DateDebEvent).ToList();
            return list;
        }

        [HttpGet("{level}&{token}")]
        public IEnumerable<EvenementDTO> Get(int level, string token)
        {
            int retrievedId = AuthentificationSecurity.RetrieveIdFromToken(token);
            if (level == 1)
                return EvenementDAO.GetExceptUser(retrievedId);    
            if(level == 2)
                return EvenementDAO.GetFromUser(retrievedId);
            if (level == 3)
                return EvenementDAO.GetModeration(retrievedId);
            return null;
        }

        [HttpGet("{id}")]
        public IEnumerable<EvenementDTO> GetFromPage(int id)
        {
            return EvenementDAO.GetFromPage(id);
        }
        
//        [HttpGet("{id}")]
//        public EvenementDTO Get(int id)
//        {
//            return EvenementDAO.Get(id);
//        }

        [HttpPost]
        public ActionResult<EvenementDTO> Post([FromBody] EvenementDTO evenement)
        {
            if (evenement.IsValid())
                return Ok(EvenementDAO.Post(evenement));
            return NotFound("Event is not valid");
        }

        [HttpPut]
        public ActionResult Put([FromBody] EvenementDTO evenement)
        {
            if(evenement.IsValid())
                return EvenementDAO.Update(evenement) ? (ActionResult) Ok() : NotFound();
            return NotFound("Event is not valid");
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            return (EvenementDAO.Delete(id)) ? (ActionResult) Ok() : NotFound();
        }
        
    }

}