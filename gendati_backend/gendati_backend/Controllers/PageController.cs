﻿using System.Collections.Generic;
using gendati_backend.DAO;
using gendati_backend.DTO;
using gendati_backend.Utility;
using Microsoft.AspNetCore.Mvc;

namespace gendati_backend.Controllers
{
    [ApiController]
    [Route("/api/page")]
    public class PageController : ControllerBase
    {
        [HttpGet]
        public ActionResult<IEnumerable<PageDTO>> Query()
        {
            return Ok(PageDAO.Query());
        }
        
        [HttpGet("{id}")]
        public ActionResult<PageDTO> Get(int id)
        {
            PageDTO pageDto = PageDAO.Get(id);

            return pageDto != null ? (ActionResult<PageDTO>) Ok(pageDto) : NotFound("This page doesn't exist!");
        }
        
        [HttpGet("{level}&{token}")]
        public List<PageDTO> Get(int level, string token)
        {
            int retrievedId = AuthentificationSecurity.RetrieveIdFromToken(token);
            if (level == 1)
                return PageDAO.GetExceptUser(retrievedId);
            if (level == 2)
                return PageDAO.GetFromUser(retrievedId);
            if (level == 3)
                return PageDAO.GetModerated(retrievedId);
            return null;
        }

        [HttpPost]
        public ActionResult<PageDTO> Post([FromBody] PageDTO pageDto)
        {
            return Ok(PageDAO.Create(pageDto));
        }
        
        [HttpPut]
        public ActionResult Put([FromBody] PageDTO pageDto)
        {
            return PageDAO.Update(pageDto) ? (ActionResult) Ok() : NotFound();
        }
        
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            if (PageDAO.Delete(id))
            {
                return Ok();
            }
            return BadRequest();
        }
    }
}