﻿using System.Collections.Generic;
using gendati_backend.DAO;
using gendati_backend.DTO;
using gendati_backend.Models;
using gendati_backend.Utility;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.JsonWebTokens;

namespace gendati_backend.Controllers
{
    [ApiController]
    [Route("/api/utilisateur")] // ATTENTION
    public class UtilisateurController : ControllerBase
    {
        [HttpGet]
        public ActionResult<IEnumerable<UtilisateurDTO>> Query()
        {
            return Ok(UtilisateurDAO.Query());
        }

        [HttpPost]
        public ActionResult<UtilisateurDTO> Post([FromBody] UtilisateurDTO utilisateurDto)
        {
            return Ok(UtilisateurDAO.Create(utilisateurDto));
        }

//        [HttpGet("{id}")]
//        public ActionResult<UtilisateurDTO> Get(int id)
//        {
//            return Ok(UtilisateurDAO.Get(id));
//        }

        [HttpGet("{token}")]
        public ActionResult<UtilisateurDTO> GetToken(string token)
        {
            int id = AuthentificationSecurity.RetrieveIdFromToken(token);
            return Ok(UtilisateurDAO.Get(id));
        }
        
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            return UtilisateurDAO.Delete(id) ? (ActionResult) Ok() : NotFound();
        }

        [HttpPut]
        public ActionResult Update([FromBody] UtilisateurDTO utilisateurDto)
        {
            return UtilisateurDAO.Update(utilisateurDto) ? (ActionResult) Ok() : NotFound();
        }

        [HttpGet("{email}&{password}")]
        public ActionResult<UtilisateurDTO> Authenticate(string email, string password)
        {
            return Ok(UtilisateurDAO.Authenticate(email, password));
        }
        
    }
}