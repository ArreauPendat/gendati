﻿using System.Collections;
using gendati_backend.DAO;
using gendati_backend.DTO;
using gendati_backend.Utility;
using Microsoft.AspNetCore.Mvc;

namespace gendati_backend.Controllers
{
    [ApiController]
    [Route("/api/suit")]
    public class SuitController : ControllerBase
    {

        [HttpPost]
        public ActionResult Create([FromBody] PageDTO pageDto)
        {
            
            if (SuitDAO.Create(pageDto))
            {
                return Ok();
            }

            return NotFound();
        }

        [HttpDelete("{idPage}&{token}")]
        public ActionResult Delete(int idPage, string token)
        {
            if (SuitDAO.Delete(idPage, AuthentificationSecurity.RetrieveIdFromToken(token)))
            {
                return Ok();
            }

            return NotFound();
        }

    }
}