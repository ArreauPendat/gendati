﻿using gendati_backend.DAO;
using gendati_backend.DTO;
using gendati_backend.Utility;
using Microsoft.AspNetCore.Mvc;

namespace gendati_backend.Controllers
{
    [ApiController]
    [Route("/api/participe")]
    public class ParticipeController : ControllerBase
    {

        [HttpPost]
        public ActionResult Insert([FromBody] ParticipeDTO participe)
        {
            if (ParticipeDAO.InsertParticipation(participe))
            {
                return Ok();
            }

            return NotFound();
        }

        [HttpDelete("{idEvent}&{token}")]
        public ActionResult Delete(int idEvent, string token)
        {
            if (ParticipeDAO.DeleteParticipation(AuthentificationSecurity.RetrieveIdFromToken(token), idEvent))
            {
                return Ok();
            }

            return NotFound();
        }
        
    }
}