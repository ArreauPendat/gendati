﻿using System.Collections;
using System.Collections.Generic;
using gendati_backend.DAO;
using gendati_backend.DTO;
using gendati_backend.Utility;
using Microsoft.AspNetCore.Mvc;

namespace gendati_backend.Controllers
{
    
    [ApiController]
    [Route("api/modere")]
    public class ModereController : ControllerBase
    {
        [HttpGet]
        public ActionResult<IEnumerable<ModereDTO>> Query()
        {
            return Ok(ModereDAO.Query());
        }

        [HttpGet("{id}&{token}")]
        public IEnumerable<UtilisateurDTO> GetModerators(int id, string token)
        {
            return ModereDAO.GetModerators(id, AuthentificationSecurity.RetrieveIdFromToken(token));
        }

        [HttpDelete("{id}&{token}")]
        public ActionResult UngrantModerator(int id, string token)
        {
            return Ok(ModereDAO.UngrantModerator(id, AuthentificationSecurity.RetrieveIdFromToken(token)));
        }
        
        [HttpPost]
        public ActionResult<ModereDTO> Post([FromBody] ModereDTO modere)
        {
            return Ok(ModereDAO.Create(modere));
        }

        [HttpPut("{id}")]
        public ActionResult GrantToAdministrator(int id, [FromBody] UtilisateurDTO user)
        {
            ModereDTO modere = new ModereDTO(AuthentificationSecurity.RetrieveIdFromToken(user.Token), id, true);
            return ModereDAO.Update(modere) ? (ActionResult) Ok() : NotFound();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            return (ModereDAO.Delete(id)) ? (ActionResult) Ok() : NotFound();
        }
    }
}