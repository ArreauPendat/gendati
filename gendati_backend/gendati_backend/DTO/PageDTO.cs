﻿using System;
using System.Data.SqlClient;
using gendati_backend.DAO;

namespace gendati_backend.DTO
{
    public class PageDTO
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public string Type { get; set; }
        public string LienSite { get; set; }
        public string LienTwitter { get; set; }
        public string LienFacebook { get; set; }
        
        public string Description { get; set; }
        
        public string UserToken { get; set; }

        public PageDTO()
        {
            
        }

        public PageDTO(int id, string nom, string type, string lienSite, string lienTwitter, string lienFacebook, string description)
        {
            Id = id;
            Nom = nom;
            Type = type;
            LienSite = lienSite;
            LienTwitter = lienTwitter;
            LienFacebook = lienFacebook;
            Description = description;
        }
        
        public PageDTO(SqlDataReader reader)
        {
            Id = Convert.ToInt32(reader[PageDAO.FIELD_ID].ToString());
            Nom = reader[PageDAO.FIELD_NOM].ToString();
            Type = reader[PageDAO.FIELD_TYPE].ToString();
            LienSite = reader[PageDAO.FIELD_SITE].ToString();
            LienTwitter = reader[PageDAO.FIELD_TWITTER].ToString();
            LienFacebook = reader[PageDAO.FIELD_FACEBOOK].ToString();
            Description = reader[PageDAO.FIELD_DESCRIPTION].ToString();
        }
    }
}