﻿using System;
using System.Data.SqlClient;
using gendati_backend.DAO;

namespace gendati_backend.DTO
{
    public class ModereDTO
    {
        public int IdUtil { set; get; }
        public int IdPage { set; get; }
        public bool EstAdmin { set; get; }
        
        public string MailUtil { set; get; }
        
        public ModereDTO()
        {
            
        }
        
        public ModereDTO(int idUtil, int idPage, bool estAdmin)
        {
            IdUtil = idUtil;
            IdPage = idPage;
            EstAdmin = estAdmin;
        }

        public ModereDTO(SqlDataReader reader)
        {
            this.IdUtil = Convert.ToInt32(reader[ModereDAO.FIELD_IDUTIL].ToString());
            this.IdPage = Convert.ToInt32(reader[ModereDAO.FIELD_IDPAGE].ToString());
            this.EstAdmin = Convert.ToBoolean(reader[ModereDAO.FIELD_ESTADMIN].ToString());
        }
        
        
    }
}