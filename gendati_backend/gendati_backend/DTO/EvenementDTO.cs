﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using gendati_backend.DAO;
using gendati_backend.Models;

namespace gendati_backend.DTO
{
    public class EvenementDTO
    {
        
        public int IdEvent { get; set; }
        public string NomEvent { get; set; }
        public int NbPersMinEvent { get; set; }
        public int NbPersMaxEvent { get; set; }
        public int IdOrga { get; set; }
        public DateTime DateDebEvent { get; set; }
        public DateTime DateFinEvent { get; set; }
        public string DescriptionEvent { get; set; }

        public EvenementDTO() { }

        public EvenementDTO(int IdEvent, string nomEvent, int nbPersMinEvent, int nbPersMaxEvent, int idOrga, DateTime dateDebEvent, DateTime dateFinEvent, string descriptionEvent)
        {
            this.IdEvent = IdEvent;
            this.NomEvent = nomEvent;
            this.NbPersMinEvent = nbPersMinEvent;
            this.NbPersMaxEvent = nbPersMaxEvent;
            this.IdOrga = idOrga;
            this.DateDebEvent = dateDebEvent;
            this.DateFinEvent = dateFinEvent;
            this.DescriptionEvent = descriptionEvent;
        }

        public EvenementDTO(SqlDataReader reader)
        {
            this.IdEvent = Convert.ToInt32(reader[EvenementDAO.FIELD_IDEVENT].ToString());
            this.NomEvent = reader[EvenementDAO.FIELD_NOMEVENT].ToString();
            this.NbPersMinEvent = Convert.ToInt32(reader[EvenementDAO.FIELD_NBPERSMINEVENT].ToString());
            this.NbPersMaxEvent = Convert.ToInt32(reader[EvenementDAO.FIELD_NBPERSMAXEVENT].ToString());
            this.IdOrga = Convert.ToInt32(reader[EvenementDAO.FIELD_IDORGA].ToString());
            this.DateDebEvent = Convert.ToDateTime(reader[EvenementDAO.FIELD_DATEDEBEVENT].ToString());
            this.DateFinEvent = Convert.ToDateTime(reader[EvenementDAO.FIELD_DATEFINEVENT].ToString());
            this.DescriptionEvent = reader[EvenementDAO.FIELD_DESCRIPTIONEVENT].ToString();
        }

        public bool IsValid()
        {
            if (this.NbPersMinEvent > this.NbPersMaxEvent) return false;

            if (this.NbPersMaxEvent > 1000 || this.NbPersMinEvent > 1000) return false;
            
            if (DateTime.Compare(this.DateDebEvent ,DateTime.Now)<0) return false;

            return true;
        }
        
    }
}