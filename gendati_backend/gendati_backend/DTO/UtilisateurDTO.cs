﻿using System;
using System.Data.SqlClient;
using gendati_backend.DAO;

namespace gendati_backend.DTO
{
    public class UtilisateurDTO
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public bool EstAdmin { get; set; }
        
        public string Password { get; set; }
        
        public string Email { get; set; }
        
        public string Token { get; set; } // JSON WEB TOKEN sauvegardé
        
        public UtilisateurDTO()
        {
            
        }

        public UtilisateurDTO(SqlDataReader reader)
        {
            Id = Convert.ToInt32(reader[UtilisateurDAO.FIELD_ID].ToString());
            Nom = reader[UtilisateurDAO.FIELD_NOM].ToString();
            Prenom = reader[UtilisateurDAO.FIELD_PRENOM].ToString();
            EstAdmin = Convert.ToBoolean(reader[UtilisateurDAO.FIELD_ESTADMIN]);
            Password = reader[UtilisateurDAO.FIELD_PASSWORD].ToString();
            Email = reader[UtilisateurDAO.FIELD_EMAIL].ToString();
        }
    }
}