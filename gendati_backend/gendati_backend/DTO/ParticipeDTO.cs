﻿using System;
using System.Data.SqlClient;
using gendati_backend.DAO;

namespace gendati_backend.DTO
{
    public class ParticipeDTO
    {
        public int IdUtil { set; get; }
        public int IdEvent { set; get; }
        
        public string UserToken { set; get; }

        public ParticipeDTO()
        {
        }

        public ParticipeDTO(int idUtil, int idEvent)
        {
            IdUtil = idUtil;
            IdEvent = idEvent;
        }

        public ParticipeDTO(SqlDataReader reader)
        {
            IdUtil = Convert.ToInt32(reader[ParticipeDAO.FIELD_IDUTIL].ToString());
            IdEvent = Convert.ToInt32(reader[ParticipeDAO.FIELD_IDEVENT].ToString());
        }
        
    }
}