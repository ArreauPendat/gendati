﻿using System.Data.SqlClient;

namespace gendati_backend.Models
{
    public class DataBase
    {
        private static readonly string DATABASE_NAME = "gendadb";
        private static readonly string USER_NAME = "bigjim";
        private static readonly string HOSTING_NAME = "genda.database.windows.net";
        private static readonly string PASSWORD = "HELHa2019";
        
        private static readonly string CONNECTION_STRING = $"User ID={USER_NAME};Password={PASSWORD};Server={HOSTING_NAME};Database={DATABASE_NAME}";

        public static SqlConnection GetConnection()
        {
            return new SqlConnection(CONNECTION_STRING);
        }
    }
}