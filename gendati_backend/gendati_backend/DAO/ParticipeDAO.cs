﻿using System.Data.SqlClient;
using gendati_backend.DTO;
using gendati_backend.Models;
using gendati_backend.Utility;
using Microsoft.EntityFrameworkCore;

namespace gendati_backend.DAO
{
    public class ParticipeDAO
    {
        public static readonly string TABLE_NAME = "participe";
        public static readonly string FIELD_IDEVENT = "idevent";
        public static readonly string FIELD_IDUTIL = "idutil";

        
        private static readonly string REQ_INSERT
            = $"INSERT INTO {TABLE_NAME} ({FIELD_IDUTIL}, {FIELD_IDEVENT}) VALUES (@{FIELD_IDUTIL}, @{FIELD_IDEVENT})";

        private static string REQ_DELETE
            = $"DELETE FROM {TABLE_NAME} WHERE {FIELD_IDUTIL} = @{FIELD_IDUTIL} AND {FIELD_IDEVENT} = @{FIELD_IDEVENT}";

        public static bool InsertParticipation(ParticipeDTO participe)
        {
            using (var connection = DataBase.GetConnection())
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                command.CommandText = REQ_INSERT;
                command.Parameters.AddWithValue($"@{FIELD_IDUTIL}", AuthentificationSecurity.RetrieveIdFromToken(participe.UserToken));
                command.Parameters.AddWithValue($"@{FIELD_IDEVENT}", participe.IdEvent);
                
                return command.ExecuteNonQuery() == 1;
            }
        }

        public static bool DeleteParticipation(int retrieveIdFromToken, int idEvent)
        {
            using (var connection = DataBase.GetConnection())
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                command.CommandText = REQ_DELETE;
                command.Parameters.AddWithValue($"@{FIELD_IDUTIL}", retrieveIdFromToken);
                command.Parameters.AddWithValue($"@{FIELD_IDEVENT}", idEvent);

                return command.ExecuteNonQuery() == 1;
            }
        }

    }
}