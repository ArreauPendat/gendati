﻿using System;
using System.Data.SqlClient;
using gendati_backend.DTO;
using gendati_backend.Models;
using gendati_backend.Utility;

namespace gendati_backend.DAO
{
    public class SuitDAO
    {
        public static readonly string TABLE_NAME = "suit";
        public static readonly string FIELD_IDUTIL = "idutil";
        public static readonly string FIELD_IDSUIVI = "idsuivi";
        public static readonly string FIELD_DATEDEB = "datedebut";

        private static readonly string REQ_POST =
            string.Format("INSERT INTO {0} ({1}, {2}, {3}) VALUES (@{1}, @{2}, @{3})",TABLE_NAME, FIELD_IDUTIL, FIELD_IDSUIVI, FIELD_DATEDEB);

        private static readonly string REQ_DELETE
            = $"DELETE FROM {TABLE_NAME} WHERE {FIELD_IDSUIVI} = @{FIELD_IDSUIVI} AND {FIELD_IDUTIL} = @{FIELD_IDUTIL}";

        public static bool Create(PageDTO pageDto)
        {
            using (SqlConnection connection = DataBase.GetConnection())
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_POST;

                command.Parameters.AddWithValue($"@{FIELD_IDUTIL}",
                    AuthentificationSecurity.RetrieveIdFromToken(pageDto.UserToken));
                command.Parameters.AddWithValue($"@{FIELD_IDSUIVI}", pageDto.Id);
                command.Parameters.AddWithValue($"@{FIELD_DATEDEB}", DateTime.Now);

                return command.ExecuteNonQuery() == 1;
            }
        }

        public static bool Delete(int idPage, int retrieveIdFromToken)
        {
            using (var connection = DataBase.GetConnection())
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                command.CommandText = REQ_DELETE;
                command.Parameters.AddWithValue($"@{FIELD_IDUTIL}", retrieveIdFromToken);
                command.Parameters.AddWithValue($"@{FIELD_IDSUIVI}", idPage);
                
                return command.ExecuteNonQuery() == 1;
            }
            
        }
    }
}