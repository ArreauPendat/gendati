﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO.Pipes;
using gendati_backend.DTO;
using gendati_backend.Models;
using gendati_backend.Utility;
using Microsoft.AspNetCore.Mvc;

namespace gendati_backend.DAO
{
    public class EvenementDAO
    {

        public static readonly string TABLE_NAME = "evenement";
        
        public static readonly string FIELD_IDEVENT = "idevent";
        public static readonly string FIELD_NOMEVENT = "nomevent";
        public static readonly string FIELD_NBPERSMINEVENT = "nbpersminevent";
        public static readonly string FIELD_NBPERSMAXEVENT = "nbpersmaxevent";
        public static readonly string FIELD_IDORGA = "idorga";
        public static readonly string FIELD_DATEDEBEVENT = "datedebevent";
        public static readonly string FIELD_DATEFINEVENT = "datefinevent";
        public static readonly string FIELD_DESCRIPTIONEVENT = "descriptionevent";

        private static readonly string REQ_QUERY = $"SELECT * FROM {TABLE_NAME} ";

        private static readonly string REQ_GET = $"SELECT * FROM {TABLE_NAME} WHERE {FIELD_IDEVENT} = @{FIELD_IDEVENT}";

        private static readonly string REQ_INSERT
            = string.Format(
                "INSERT INTO {0} ({1}, {2}, {3}, {4}, {5}, {6}, {7}) OUTPUT Inserted.{8} VALUES (@{1}, @{2}, @{3}, @{4}, @{5}, @{6}, @{7})",
                TABLE_NAME, FIELD_NOMEVENT, FIELD_NBPERSMINEVENT, FIELD_NBPERSMAXEVENT, 
                FIELD_IDORGA, FIELD_DATEDEBEVENT, FIELD_DATEFINEVENT, FIELD_DESCRIPTIONEVENT, FIELD_IDEVENT
                );

        private static readonly string REQ_UPDATE
            = string.Format("UPDATE {0} SET {1} = @{1}, {2} = @{2}, {3} = @{3}, {4} = @{4}, {5} = @{5}, {6} = @{6}, {7} = @{7} WHERE {8} = @{8}",
                TABLE_NAME, FIELD_NOMEVENT, FIELD_DATEDEBEVENT, FIELD_DATEFINEVENT, FIELD_IDORGA, FIELD_NBPERSMAXEVENT, FIELD_NBPERSMINEVENT, FIELD_DESCRIPTIONEVENT, FIELD_IDEVENT
                );

        private static readonly string REQ_DELETE
            = string.Format("DELETE FROM {0} WHERE {1} = @{1}", TABLE_NAME, FIELD_IDEVENT);

        private static readonly string REQ_DELETEINPARTICIPATION
            = string.Format("DELETE FROM {0} WHERE {0}.{1} = @{1}", ParticipeDAO.TABLE_NAME, ParticipeDAO.FIELD_IDEVENT);

        private static readonly string REQ_GETFROMUSER
            = string.Format("SELECT * FROM {0} INNER JOIN {1} ON {0}.{2} = {1}.{3} WHERE {1}.{4} = @{4}", TABLE_NAME, ParticipeDAO.TABLE_NAME, FIELD_IDEVENT, ParticipeDAO.FIELD_IDEVENT, ParticipeDAO.FIELD_IDUTIL);

        private static readonly string REQ_GETEXCEPTUSER
            = string.Format("SELECT * FROM {0} LEFT JOIN {1} ON {0}.{2} = {1}.{3} WHERE {4} is null OR not @{4} in (select {4} from {1} where {1}.{3} = {0}.{2})", 
                TABLE_NAME, ParticipeDAO.TABLE_NAME, FIELD_IDEVENT, ParticipeDAO.FIELD_IDEVENT, ParticipeDAO.FIELD_IDUTIL);

        private static readonly string REQ_GETMODERATION
            = string.Format("SELECT * FROM {0} INNER JOIN {1} ON {0}.{2} = {1}.{3} WHERE {1}.{4} = @{5}",
                TABLE_NAME, ModereDAO.TABLE_NAME, FIELD_IDORGA, ModereDAO.FIELD_IDPAGE, ModereDAO.FIELD_IDUTIL, UtilisateurDAO.FIELD_ID);

        private static readonly string REQ_GETFROMPAGE
            = string.Format("SELECT * FROM {0} WHERE {1} = @{1}", TABLE_NAME, FIELD_IDORGA);

        public static List<EvenementDTO> Query()
        {
            List<EvenementDTO> evenements = new List<EvenementDTO>();

            using (SqlConnection connection = DataBase.GetConnection())
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_QUERY;

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    evenements.Add(new EvenementDTO(reader));
                }
            }

            return evenements;
        }

        public static EvenementDTO Get(int id)
        {

            using (SqlConnection connection = DataBase.GetConnection())
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_GET;

                command.Parameters.AddWithValue($"@{FIELD_IDEVENT}", id);

                SqlDataReader reader = command.ExecuteReader();

                return (reader.Read())? new EvenementDTO(reader) : null;
            }
            
        }

        public static EvenementDTO Post(EvenementDTO evenement)
        {
            using (SqlConnection connection = DataBase.GetConnection())
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_INSERT;

                command.Parameters.AddWithValue($"@{FIELD_NOMEVENT}", evenement.NomEvent);
                command.Parameters.AddWithValue($"@{FIELD_DATEDEBEVENT}", evenement.DateDebEvent);
                command.Parameters.AddWithValue($"@{FIELD_DATEFINEVENT}", evenement.DateFinEvent);
                command.Parameters.AddWithValue($"@{FIELD_IDORGA}", evenement.IdOrga);
                command.Parameters.AddWithValue($"@{FIELD_NBPERSMAXEVENT}", evenement.NbPersMaxEvent);
                command.Parameters.AddWithValue($"@{FIELD_NBPERSMINEVENT}", evenement.NbPersMinEvent);
                command.Parameters.AddWithValue($"@{FIELD_DESCRIPTIONEVENT}", evenement.DescriptionEvent);

                evenement.IdEvent = (int) command.ExecuteScalar();

                return evenement;

            }
        }

        public static bool Update(EvenementDTO evenement)
        {
            using (SqlConnection connection = DataBase.GetConnection())
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_UPDATE;

                command.Parameters.AddWithValue($"@{FIELD_NOMEVENT}", evenement.NomEvent);
                command.Parameters.AddWithValue($"@{FIELD_NBPERSMINEVENT}", evenement.NbPersMinEvent);
                command.Parameters.AddWithValue($"@{FIELD_NBPERSMAXEVENT}", evenement.NbPersMaxEvent);
                command.Parameters.AddWithValue($"@{FIELD_IDORGA}", evenement.IdOrga);
                command.Parameters.AddWithValue($"@{FIELD_DATEDEBEVENT}", evenement.DateDebEvent);
                command.Parameters.AddWithValue($"@{FIELD_DATEFINEVENT}", evenement.DateFinEvent);
                command.Parameters.AddWithValue($"@{FIELD_DESCRIPTIONEVENT}", evenement.DescriptionEvent);
                command.Parameters.AddWithValue($"@{FIELD_IDEVENT}", evenement.IdEvent);

                return command.ExecuteNonQuery() == 1;
            }
            
        }

        public static bool Delete(int id)
        {
            using (SqlConnection connection = DataBase.GetConnection())
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                command.CommandText = REQ_DELETEINPARTICIPATION;
                command.Parameters.AddWithValue($"@{ParticipeDAO.FIELD_IDEVENT}", id);
                if (command.ExecuteNonQuery() != 1) return false;
                
                command.CommandText = REQ_DELETE;
                command.Parameters.AddWithValue($"@{FIELD_IDEVENT}", id);

                return command.ExecuteNonQuery() == 1;
            }
        }

        public static List<EvenementDTO> GetFromUser(int retrievedIdFromToken)
        {
            
            List<EvenementDTO> events = new List<EvenementDTO>();
            
            using (var connection = DataBase.GetConnection())
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                
                command.CommandText = REQ_GETFROMUSER;
                command.Parameters.AddWithValue($"@{ParticipeDAO.FIELD_IDUTIL}", retrievedIdFromToken);
                
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    events.Add(new EvenementDTO(reader));
                }
                
            }

            return events;
        }

        public static IEnumerable<EvenementDTO> GetExceptUser(int retrievedId)
        {
            List<EvenementDTO> events = new List<EvenementDTO>();

            using (var connection = DataBase.GetConnection())
            {
                
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                command.CommandText = REQ_GETEXCEPTUSER;
                command.Parameters.AddWithValue($"@{ParticipeDAO.FIELD_IDUTIL}", retrievedId);
                
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    events.Add(new EvenementDTO(reader));
                }

            }
            
            return events;
        }
        
        public static IEnumerable<EvenementDTO> GetModeration(int retrievedId)
        {
            List<EvenementDTO> events = new List<EvenementDTO>();

            using (var connection = DataBase.GetConnection())
            {
                
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                command.CommandText = REQ_GETMODERATION;
                command.Parameters.AddWithValue($"@{UtilisateurDAO.FIELD_ID}", retrievedId);
                
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    events.Add(new EvenementDTO(reader));
                }

            }
            
            return events;
        }

        public static IEnumerable<EvenementDTO> GetFromPage(int idPage)
        {
            List<EvenementDTO> events = new List<EvenementDTO>();

            using (var connection = DataBase.GetConnection())
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_GETFROMPAGE;
                command.Parameters.AddWithValue($"@{FIELD_IDORGA}", idPage);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    events.Add(new EvenementDTO(reader));
                }
            }
            
            return events;
        }
    }
}