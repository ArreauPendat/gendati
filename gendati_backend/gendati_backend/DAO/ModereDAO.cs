﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using gendati_backend.DTO;
using gendati_backend.Models;
using gendati_backend.Utility;

namespace gendati_backend.DAO
{
    public class ModereDAO
    {
        public static readonly string TABLE_NAME = "modere";
        public static readonly string FIELD_IDUTIL = "idutil";
        public static readonly string FIELD_IDPAGE = "idpage";
        public static readonly string FIELD_ESTADMIN = "estadmin";

        private static readonly string REQ_QUERY = $"SELECT * FROM {TABLE_NAME}";
        private static readonly string REQ_GET = REQ_QUERY + $" WHERE {FIELD_IDUTIL} = @{FIELD_IDUTIL}";

        private static readonly string REQ_POST =
            $"INSERT INTO {TABLE_NAME} ({FIELD_IDUTIL}, {FIELD_IDPAGE}, {FIELD_ESTADMIN}) VALUES (@{FIELD_IDUTIL}, @{FIELD_IDPAGE}, @{FIELD_ESTADMIN})";

        private static readonly string REQ_GETUSERFROMMAIL =
            $"SELECT * FROM {UtilisateurDAO.TABLE_NAME} WHERE {UtilisateurDAO.FIELD_EMAIL} = @{UtilisateurDAO.FIELD_EMAIL}";

        private static readonly string REQ_UPDATE =
            string.Format("UPDATE {0} SET {1} = @{1} WHERE {2} = @{2} AND {3} = @{3}",
                TABLE_NAME, FIELD_ESTADMIN, FIELD_IDUTIL, FIELD_IDPAGE);

        private static readonly string REQ_DELETE =
            $"DELETE FROM {TABLE_NAME} WHERE @{FIELD_IDUTIL} = @{FIELD_IDUTIL}";

        private static readonly string REQ_GETMODERATORS
            = string.Format("SELECT {1}.{5} as 'isAdmin', * FROM {0} INNER JOIN {1} ON {1}.{2} = {0}.{3} WHERE {1}.{4} = @{4}",
                UtilisateurDAO.TABLE_NAME, TABLE_NAME, UtilisateurDAO.FIELD_ID, FIELD_IDUTIL, FIELD_IDPAGE, FIELD_ESTADMIN);

        private static readonly string REQ_UNGRANT
            =$"DELETE FROM {TABLE_NAME} WHERE {FIELD_IDPAGE} = @{FIELD_IDPAGE} AND {FIELD_IDUTIL} = @{FIELD_IDUTIL}";

        public static IEnumerable<ModereDTO> Query()
        {
            List<ModereDTO> modere = new List<ModereDTO>();

            using (var connection = DataBase.GetConnection())
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_QUERY;

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    modere.Add(new ModereDTO(reader));
                }
            }

            return modere;
        }
        
        public static ModereDTO Create(ModereDTO modere)
        {
            using (var connection = DataBase.GetConnection())
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                if (modere.MailUtil != null)
                {
                    command.CommandText = REQ_GETUSERFROMMAIL;
                    command.Parameters.AddWithValue($"@{UtilisateurDAO.FIELD_EMAIL}", modere.MailUtil);
                    SqlDataReader reader = command.ExecuteReader();
                    if (!reader.Read()) return null;
                    UtilisateurDTO user = new UtilisateurDTO(reader);
                    modere.IdUtil = user.Id;
                    reader.Close();
                }
                
                command.CommandText = REQ_POST;
                command.Parameters.AddWithValue($"@{FIELD_ESTADMIN}", modere.EstAdmin);
                command.Parameters.AddWithValue($"@{FIELD_IDPAGE}", modere.IdPage);
                command.Parameters.AddWithValue($"@{FIELD_IDUTIL}", modere.IdUtil);

                command.ExecuteNonQuery();
            }

            return modere;
        }
        
        public static ModereDTO Get(int id)
        {
            using (var connection = DataBase.GetConnection())
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_GET;

                command.Parameters.AddWithValue($"@{FIELD_IDUTIL}", id);

                SqlDataReader reader = command.ExecuteReader();

                return (reader.Read()) ? new ModereDTO(reader) : null;
            }
        }
        
        public static bool Delete(int id)
        {
            using (var connection = DataBase.GetConnection())
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_DELETE;

                command.Parameters.AddWithValue($"@{FIELD_IDUTIL}", id);

                return command.ExecuteNonQuery() == 1;
            }
        }
        
        public static bool Update(ModereDTO modere)
        {
            using (var connection = DataBase.GetConnection())
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_UPDATE;
                
                command.Parameters.AddWithValue($"@{FIELD_ESTADMIN}", modere.EstAdmin);
                command.Parameters.AddWithValue($"@{FIELD_IDUTIL}", modere.IdUtil);
                command.Parameters.AddWithValue($"@{FIELD_IDPAGE}", modere.IdPage);

                return command.ExecuteNonQuery() == 1;
            }
        }

        public static List<UtilisateurDTO> GetModerators(int idPage, int retrieved)
        {
            List<UtilisateurDTO> moderators = new List<UtilisateurDTO>();

            using (var connection = DataBase.GetConnection())
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                command.CommandText = REQ_GETMODERATORS;
                command.Parameters.AddWithValue($"@{FIELD_IDPAGE}", idPage);
                
                SqlDataReader reader = command.ExecuteReader();
                UtilisateurDTO user;
                while (reader.Read())
                {
                    user = new UtilisateurDTO(reader);
                    user.Token = AuthentificationSecurity.GenerateToken(user.Id.ToString());
                    user.Password = null;
                    //isAdmin is used here to know if the user is a administrator of the page
                    if (Convert.ToBoolean(reader["isAdmin"])) user.EstAdmin = true;
                    else user.EstAdmin = false;
                    if (user.Id == retrieved && user.EstAdmin) user.Id = 1;
                    else user.Id = 0;
                    moderators.Add(user);
                }
            }
            
            return moderators;
        }

        public static bool UngrantModerator(int idPage, int retrieveIdFromToken)
        {
            using(var connection = DataBase.GetConnection()) {
                connection.Open();
                
                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_UNGRANT;
                command.Parameters.AddWithValue($"@{FIELD_IDPAGE}", idPage);
                command.Parameters.AddWithValue($"@{FIELD_IDUTIL}", retrieveIdFromToken);
                
                return command.ExecuteNonQuery() == 1;
            }
        }
    }
}