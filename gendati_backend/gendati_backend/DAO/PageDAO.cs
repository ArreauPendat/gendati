﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using gendati_backend.DTO;
using gendati_backend.Models;
using gendati_backend.Utility;
using Microsoft.CodeAnalysis;

namespace gendati_backend.DAO
{
    public class PageDAO
    {
        private static readonly string TABLE_NAME = "page";
        public static readonly string FIELD_ID = "idpage";
        public static readonly string FIELD_NOM = "nompage";
        public static readonly string FIELD_TYPE = "typepage";
        public static readonly string FIELD_SITE = "liensitepage";
        public static readonly string FIELD_TWITTER = "lientwitterpage";
        public static readonly string FIELD_FACEBOOK = "lienfacebookpage";
        public static readonly string FIELD_DESCRIPTION = "description";
        
        private static readonly string REQ_QUERY = $"SELECT * FROM {TABLE_NAME}";
        private static readonly string REQ_GET = REQ_QUERY + $" WHERE {FIELD_ID} = @{FIELD_ID}";
        private static readonly string REQ_INSERT = String.Format("INSERT INTO {0}({1}, {2}, {3}, {4}, {5}, {6}) OUTPUT Inserted.{7} VALUES(@{1}, @{2}, @{3}, @{4}, @{5}, @{6})", TABLE_NAME, FIELD_NOM, FIELD_TYPE, FIELD_SITE,FIELD_TWITTER,FIELD_FACEBOOK, FIELD_DESCRIPTION, FIELD_ID);
        private static readonly string REQ_UPDATE = String.Format("UPDATE {0} SET {1} = @{1}, {2} = @{2}, {3} = @{3}, {4} = @{4}, {5} = @{5}, {6} = @{6} WHERE {7} = @{7}", TABLE_NAME, FIELD_NOM, FIELD_TYPE, FIELD_SITE,FIELD_TWITTER,FIELD_FACEBOOK, FIELD_DESCRIPTION, FIELD_ID);
        private static readonly string REQ_DELETE = $"DELETE FROM {TABLE_NAME} WHERE {FIELD_ID} = @{FIELD_ID}";

        private static readonly string REQ_DELETEINSUIT =
            $"DELETE FROM {SuitDAO.TABLE_NAME} WHERE {SuitDAO.FIELD_IDSUIVI} = @{SuitDAO.FIELD_IDSUIVI}";

        private static readonly string REQ_GETEXCEPTUSER
            = string.Format("SELECT * FROM {0} LEFT JOIN {1} ON {0}.{2} = {1}.{3} WHERE {1}.{3} is null OR not @{4} in (select {4} from {1} where {1}.{3} = {0}.{2})",
                TABLE_NAME, SuitDAO.TABLE_NAME, FIELD_ID, SuitDAO.FIELD_IDSUIVI, SuitDAO.FIELD_IDUTIL);
        
        private static readonly string REQ_GETFROMUSER
            = string.Format("SELECT * FROM {0} INNER JOIN {1} ON {0}.{2} = {1}.{3} WHERE {1}.{4} = @{4}",
                TABLE_NAME, SuitDAO.TABLE_NAME, FIELD_ID, SuitDAO.FIELD_IDSUIVI, SuitDAO.FIELD_IDUTIL);

        private static readonly string REQ_GETMODERATED =
            $"SELECT * FROM {TABLE_NAME} INNER JOIN {ModereDAO.TABLE_NAME} ON {TABLE_NAME}.{FIELD_ID} = {ModereDAO.TABLE_NAME}.{ModereDAO.FIELD_IDPAGE} WHERE {ModereDAO.FIELD_IDUTIL} = @{ModereDAO.FIELD_IDUTIL}";

        // GET All
        public static IEnumerable<PageDTO> Query()
        {
            List<PageDTO> pages = new List<PageDTO>();
            using (var connection = DataBase.GetConnection())
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_QUERY;

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    pages.Add(new PageDTO(reader));
                }
            }
            return pages;
        }

        // GET par ID
        public static PageDTO Get(int id)
        {
            using (var connection = DataBase.GetConnection())
            {
                connection.Open();
        
                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_GET;
        
                command.Parameters.AddWithValue($"@{FIELD_ID}", id);
        
                SqlDataReader reader = command.ExecuteReader();
        
                return reader.Read() ? new PageDTO(reader) : null;
            }
        }
        
        public static List<PageDTO> GetExceptUser(int retrievedId)
        {
            List<PageDTO> pages = new List<PageDTO>();
            
            using (var connection = DataBase.GetConnection())
            {
                connection.Open();
        
                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_GETEXCEPTUSER;
                command.Parameters.AddWithValue($"@{SuitDAO.FIELD_IDUTIL}", retrievedId);
        
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    pages.Add(new PageDTO(reader));    
                }
                
            }

            return pages;
        }
        
        public static List<PageDTO> GetFromUser(int retrievedId)
        {
            List<PageDTO> pages = new List<PageDTO>();
            
            using (var connection = DataBase.GetConnection())
            {
                connection.Open();
        
                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_GETFROMUSER;
                command.Parameters.AddWithValue($"@{SuitDAO.FIELD_IDUTIL}", retrievedId);
        
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    pages.Add(new PageDTO(reader));
                }
            }

            return pages;
        }

        public static List<PageDTO> GetModerated(int retrievedId)
        {
            List<PageDTO> pages = new List<PageDTO>();
            using (var connection = DataBase.GetConnection())
            {
                connection.Open();
        
                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_GETMODERATED;
                command.Parameters.AddWithValue($"@{ModereDAO.FIELD_IDUTIL}", retrievedId);
        
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    pages.Add(new PageDTO(reader));
                }
            }

            return pages;
        }
        
        public static bool Delete(int id)
        {
            bool hasBeenDeleted = false;

            using (SqlConnection connection = DataBase.GetConnection())
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                
                //Delete in table suit in cascade
                command.CommandText = REQ_DELETEINSUIT;
                command.Parameters.AddWithValue($"@{SuitDAO.FIELD_IDSUIVI}", id);
                if (command.ExecuteNonQuery() != 1) return false;
                
                //Delete in table evenement in cascade
                List<EvenementDTO> events = new List<EvenementDTO>();
                events = EvenementDAO.GetFromPage(id) as List<EvenementDTO>;
                for (int i = 0; i < events.Count; i++)
                {
                    EvenementDAO.Delete(events[i].IdEvent);
                }
                
                command.CommandText = REQ_DELETE;
                command.Parameters.AddWithValue($@"{FIELD_ID}", id);

                // Nombre de lignes qui ont été affectées donc si = 1 c'est bon
                hasBeenDeleted = command.ExecuteNonQuery() == 1;
            }
            
            return hasBeenDeleted;
        }
        
        public static PageDTO Create(PageDTO pageDto)
        {
            using (var connection = DataBase.GetConnection())
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_INSERT;
                
                command.Parameters.AddWithValue($"@{FIELD_NOM}", pageDto.Nom);
                command.Parameters.AddWithValue($"@{FIELD_TYPE}", pageDto.Type);
                command.Parameters.AddWithValue($"@{FIELD_SITE}", pageDto.LienSite);
                command.Parameters.AddWithValue($"@{FIELD_TWITTER}", pageDto.LienTwitter);
                command.Parameters.AddWithValue($"@{FIELD_FACEBOOK}", pageDto.LienFacebook);
                command.Parameters.AddWithValue($"@{FIELD_DESCRIPTION}", pageDto.Description);
                
                pageDto.Id = (int) command.ExecuteScalar();
                        
                //Add admin permission on this page for the creator
                ModereDTO modere = new ModereDTO(AuthentificationSecurity.RetrieveIdFromToken(pageDto.UserToken), pageDto.Id, true);
                ModereDAO.Create(modere);

            }

            return pageDto;
        }
        
        public static bool Update(PageDTO pageDto)
        {
            using (SqlConnection connection = DataBase.GetConnection())
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_UPDATE;

                command.Parameters.AddWithValue($"@{FIELD_ID}", pageDto.Id);
                command.Parameters.AddWithValue($"@{FIELD_NOM}", pageDto.Nom);
                command.Parameters.AddWithValue($"@{FIELD_TYPE}", pageDto.Type);
                command.Parameters.AddWithValue($"@{FIELD_SITE}", pageDto.LienSite);
                command.Parameters.AddWithValue($"@{FIELD_TWITTER}", pageDto.LienTwitter);
                command.Parameters.AddWithValue($"@{FIELD_FACEBOOK}", pageDto.LienFacebook);
                command.Parameters.AddWithValue($"@{FIELD_DESCRIPTION}", pageDto.Description);

                return command.ExecuteNonQuery() == 1;
            }

            return false;
        }
    }
}