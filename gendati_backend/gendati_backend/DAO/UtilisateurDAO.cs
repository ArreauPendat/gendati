﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using gendati_backend.DTO;
using gendati_backend.Models;
using gendati_backend.Utility;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;

namespace gendati_backend.DAO
{
    public class UtilisateurDAO
    {

        public static readonly string TABLE_NAME = "utilisateur";

        public static readonly string FIELD_ID = "idutil";
        public static readonly string FIELD_NOM = "nom";
        public static readonly string FIELD_PRENOM = "prenom";
        public static readonly string FIELD_ESTADMIN = "estadmin";
        public static readonly string FIELD_PASSWORD = "password";
        public static readonly string FIELD_EMAIL = "email";

        private static readonly string REQ_QUERY =
            $"SELECT * FROM {TABLE_NAME}";

        private static readonly string REQ_POST =
            $"INSERT INTO {TABLE_NAME}" +
            $"({FIELD_NOM},{FIELD_PRENOM},{FIELD_ESTADMIN},{FIELD_PASSWORD},{FIELD_EMAIL})" +
            $" OUTPUT Inserted.{FIELD_ID} VALUES" +
            $" (@{FIELD_NOM},@{FIELD_PRENOM},@{FIELD_ESTADMIN},@{FIELD_PASSWORD},@{FIELD_EMAIL})";

        private static readonly string REQ_GET =
            REQ_QUERY + $" WHERE {FIELD_ID} = @{FIELD_ID}";

        private static readonly string REQ_DELETE =
            $"DELETE FROM {TABLE_NAME} WHERE {FIELD_ID} = @{FIELD_ID}";

        private static readonly string REQ_UPDATE =
            $"UPDATE {TABLE_NAME} SET "+
            $"{FIELD_ESTADMIN} = @{FIELD_ESTADMIN}, " +
            $"{FIELD_PASSWORD} = @{FIELD_PASSWORD}, " +
            $"{FIELD_EMAIL} = @{FIELD_EMAIL}" +
            $" WHERE {FIELD_ID} = @{FIELD_ID}";

        private static readonly string REQ_CONNEXION =
            string.Format("SELECT * FROM {0} WHERE {1} = @{1} AND {2} = @{2}", TABLE_NAME, FIELD_EMAIL, FIELD_PASSWORD);

        public static IEnumerable<UtilisateurDTO> Query()
        {
            List<UtilisateurDTO> utilisateurs = new List<UtilisateurDTO>();

            using (var connection = DataBase.GetConnection())
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_QUERY;

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    UtilisateurDTO utilisateurDto = new UtilisateurDTO(reader);
                    utilisateurDto.Token = AuthentificationSecurity.GenerateToken(utilisateurDto.Id.ToString());
                    utilisateurDto.Password = null;
                    utilisateurs.Add(utilisateurDto);
                }
            }

            return utilisateurs;
        }

        public static UtilisateurDTO Create(UtilisateurDTO utilisateurDto)
        {
            using (var connection = DataBase.GetConnection())
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_POST;

                command.Parameters.AddWithValue($"@{FIELD_NOM}", utilisateurDto.Nom);
                command.Parameters.AddWithValue($"@{FIELD_PRENOM}", utilisateurDto.Prenom);
                command.Parameters.AddWithValue($"@{FIELD_ESTADMIN}", utilisateurDto.EstAdmin);
                command.Parameters.AddWithValue($"@{FIELD_PASSWORD}", AuthentificationSecurity.Encrypt(utilisateurDto.Password));
                command.Parameters.AddWithValue($"@{FIELD_EMAIL}", utilisateurDto.Email);

                //utilisateurDto.Id = (int) command.ExecuteScalar();
                command.ExecuteScalar();
            }

            return utilisateurDto;
        }

        public static UtilisateurDTO Get(int id)
        {
            using (var connection = DataBase.GetConnection())
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_GET;

                command.Parameters.AddWithValue($"@{FIELD_ID}", id);

                SqlDataReader reader = command.ExecuteReader();

                if (!reader.Read()) return null;
                
                UtilisateurDTO utilisateurDto = new UtilisateurDTO(reader);
                
                utilisateurDto.Token = AuthentificationSecurity.GenerateToken(utilisateurDto.Id.ToString());
                utilisateurDto.Password = null;
                utilisateurDto.Id = 0;
                return utilisateurDto;

            }
        }

        public static bool Delete(int id)
        {
            using (var connection = DataBase.GetConnection())
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_DELETE;

                command.Parameters.AddWithValue($"@{FIELD_ID}", id);

                return command.ExecuteNonQuery() == 1;
            }
        }

        public static bool Update(UtilisateurDTO utilisateurDto)
        {
            using (var connection = DataBase.GetConnection())
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_UPDATE;
                
                command.Parameters.AddWithValue($"@{FIELD_ESTADMIN}", utilisateurDto.EstAdmin);
                command.Parameters.AddWithValue($"@{FIELD_PASSWORD}", AuthentificationSecurity.Encrypt(utilisateurDto.Password));
                command.Parameters.AddWithValue($"@{FIELD_EMAIL}", utilisateurDto.Email);
                command.Parameters.AddWithValue($"@{FIELD_ID}", AuthentificationSecurity.RetrieveIdFromToken(utilisateurDto.Token));

                return command.ExecuteNonQuery() == 1;
            }
        }

        
        public static UtilisateurDTO Authenticate(string email, string password)
        {
            using (SqlConnection connection = DataBase.GetConnection())
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_CONNEXION;

                command.Parameters.AddWithValue($"@{FIELD_EMAIL}", email);
                command.Parameters.AddWithValue($"@{FIELD_PASSWORD}", AuthentificationSecurity.Encrypt(password));

                SqlDataReader reader = command.ExecuteReader();

                if (!reader.Read())
                { return null; }
                
                UtilisateurDTO utilisateur = new UtilisateurDTO(reader);
                
                utilisateur.Token = AuthentificationSecurity.GenerateToken(utilisateur.Id.ToString());
                utilisateur.Password = null;
                utilisateur.Id = 0;
                
                return utilisateur;
            }
        }
    }
}