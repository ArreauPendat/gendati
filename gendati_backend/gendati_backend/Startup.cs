﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using gendati_backend.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace gendati_backend
{
    public class Startup
    {
        
        private static readonly string PATH_ANGULAR_APP = "AngularApp/js";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddSpaStaticFiles(spa=>spa.RootPath = PATH_ANGULAR_APP);
            services.AddCors();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            DataBase.GetConnection();
            app.UseSpaStaticFiles();
            
            app.UseHttpsRedirection();
            app.UseMvc();
            
            app.UseCors();
            
            app.UseSpa(spa => spa.Options.SourcePath = PATH_ANGULAR_APP);

        }
    }
}