import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {UtilisateurService} from "./user/Services/utilisateur.service";
import {UtilisateurConnectedService} from "./user/Services/utilisateur-connected.service";
import {UtilisateurDTO} from "./user/DTO/utilisateur-dto";
import {Event, EventList} from './model/Event';
import {LocalStorageManager} from "./utility/local-storage-manager";
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {

  private subscriptions: Subscription[] = [];
  public connected_user:UtilisateurDTO = null;
  private events: EventList = [];

  constructor(public utilisateurService :UtilisateurService,
              public utilisateurConnected : UtilisateurConnectedService,
              public router : Router) {}

  ngOnInit() {
    this.listenToUtilisateurConnected();
    this.listenToChangePassword();
    this.listenToChangeMail();
    this.connectUserByToken();
  }

  ngOnDestroy(): void {
    for (let i = this.subscriptions.length - 1; i >= 0; i--) {
      const subscription = this.subscriptions[i];
      subscription && subscription.unsubscribe();
      this.subscriptions.pop();
    }
  }


  listenToUtilisateurConnected() {
    this.subscriptions.push(
      this.utilisateurConnected
        .$utilisateurConnected
        .subscribe(utilisateur => {
          this.connectUtilisateur(utilisateur);
        })
    );
  }

  connectUtilisateur(utilisateur: UtilisateurDTO) {
    this.subscriptions.push(
      this.utilisateurService
        .getConnexion(utilisateur)
        .subscribe(answer =>
        {
          if(answer == null) this.utilisateurConnected.notifyProblem(1);
          else {
            this.connected_user = answer;
            LocalStorageManager.setToken(this.connected_user.token);
            this.router.navigate([("/page/list")]);
          }
        })
    );
  }

  private connectUserByToken() {
    let token: string = LocalStorageManager.getToken();
    if (token === "" || token == null) return;
    this.subscriptions.push(
      this.utilisateurService.connectWithToken(token).subscribe(
        answer => {
          this.connected_user = answer;
        }
      )
    );
  }

  private listenToChangePassword() {
    this.subscriptions.push(
      this.utilisateurService.$changePassword.subscribe(password => {
        this.connected_user.password = password;
        this.subscriptions.push(
          this.utilisateurService.put(this.connected_user).subscribe()
        )
      })
    );
  }

  private listenToChangeMail() {
    this.subscriptions.push(
      this.utilisateurService.$changeMail.subscribe(mail => {
        this.connected_user.email = mail;
        this.subscriptions.push(
          this.utilisateurService.put(this.connected_user).subscribe()
        )
      })
    );
  }

}


