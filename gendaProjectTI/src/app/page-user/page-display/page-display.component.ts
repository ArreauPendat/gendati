import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Page} from "../../model/page";
import {Subscription} from "rxjs";
import {PageService} from "../Services/page.service";
import {PageDto} from "../DTO/page-dto";
import {Event, EventList} from "../../model/Event";
import {EvenementService} from "../../event/Service/evenement.service";

@Component({
  selector: 'app-page-display',
  templateUrl: './page-display.component.html',
  styleUrls: ['./page-display.component.css']
})
export class PageDisplayComponent implements OnInit {

  FACEBOOK_LINK:string;
  TWITTER_LINK:string;
  SITE_LINK:string;

  private _page:Page;

  get page(): Page {
    return this._page;
  }

  @Input()
  set page(value: Page) {
    this._page = value;
  }

  constructor(public eventService : EvenementService) { }

  ngOnInit() {
    this.FACEBOOK_LINK = this.page.lienFacebook;
    this.TWITTER_LINK = this.page.lienTwitter;
    this.SITE_LINK = this.page.lienSite;
    console.log(this.events);
  }

  linkToPage(link: string) {
    window.open(link);
  }

  private _events:EventList = [];

  get events(): Event[] {
    return this._events;
  }

  @Input()
  set events(value: Event[]) {
    this._events = value;
  }

  notifyEventSelected(event: Event) {
    this.eventService.notifyEventSelected(event);
  }
}
