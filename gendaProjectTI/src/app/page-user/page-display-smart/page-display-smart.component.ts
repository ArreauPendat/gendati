import {Component, OnDestroy, OnInit} from '@angular/core';
import {PageService} from "../Services/page.service";
import {Subscription} from "rxjs";
import {PageDto} from "../DTO/page-dto";
import {LocalStorageManager} from "../../utility/local-storage-manager";
import {Page} from "../../model/page";
import {EvenementService} from "../../event/Service/evenement.service";
import {Event, EventList} from "../../model/Event";

@Component({
  selector: 'app-page-display-smart',
  templateUrl: './page-display-smart.component.html',
  styleUrls: ['./page-display-smart.component.css']
})
export class PageDisplaySmartComponent implements OnInit, OnDestroy {

  private subscriptions: Subscription[] = [];
  private pageSelected: PageDto;
  private events: EventList = [];

  constructor(public pageService : PageService, public eventService : EvenementService) {
  }

  ngOnInit() {
    this.listenToPageSelected();
    this.loadEventsFromPage();
    console.log(this.events);
  }

  ngOnDestroy(): void {
    for (let i = this.subscriptions.length - 1; i >= 0; i--) {
      const subscription = this.subscriptions[i];
      subscription && subscription.unsubscribe();
      this.subscriptions.pop();
    }
  }

  listenToPageSelected() {
    this.subscriptions.push(
      this.pageService
        .$pageSelected
        .subscribe(page => this.pageSelected = page)
    );
  }

  loadEventsFromPage(){
    const sub = this.eventService
      .queryEventsPage(this.pageSelected.id)
      .subscribe(events => {
        this.events = events.map(events => new Event().fromEvenementDTO(events))
      });
    this.subscriptions.push(sub);
  }

}
