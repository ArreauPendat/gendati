import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageDisplaySmartComponent } from './page-display-smart.component';

describe('PageDisplaySmartComponent', () => {
  let component: PageDisplaySmartComponent;
  let fixture: ComponentFixture<PageDisplaySmartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageDisplaySmartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageDisplaySmartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
