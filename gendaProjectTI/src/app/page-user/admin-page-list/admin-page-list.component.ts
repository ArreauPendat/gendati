import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Page, PageList} from "../../model/page";
import {PageService} from "../Services/page.service";

@Component({
  selector: 'app-admin-page-list',
  templateUrl: './admin-page-list.component.html',
  styleUrls: ['./admin-page-list.component.css']
})
export class AdminPageListComponent implements OnInit {

  // Select fields
  readonly OPTION_ALL:string = "All";
  readonly OPTION_ENTREPRISE:string = "Company";
  readonly OPTION_ORGANISATION:string = "Organization";
  readonly OPTION_PARTICULIER:string = "Particular";

  nameSearched:string;
  typeSearched:string = this.OPTION_ALL;

  @Output()
  reloadPages:EventEmitter<Page> = new EventEmitter<Page>();

  emitReloadPages(){
    this.reloadPages.next();
  }
  sortByWhat: string = "asc";

  constructor(public pageService : PageService) { }

  ngOnInit() {
  }

  notifyPageSelected(page: Page) {
    this.pageService.notifyPageSelected(page);
  }

  private _pages:PageList = [];
  get pages(): Page[] {
    return this._pages;
  }

  @Input()
  set pages(value: Page[]) {
    this._pages = value;
  }
}
