import { Pipe, PipeTransform } from '@angular/core';
import {Page} from "../../model/page";

@Pipe({
  name: 'filterPagesNames'
})
export class FilterPagesNamesPipe implements PipeTransform {

  transform(strings: Page[], filter: string): Page[] {
    if(!filter)
    {
      return strings;
    }
    const filterLowered = filter.toLowerCase();
    return strings.filter(
      value => value.nom.toLowerCase().includes(filterLowered)
    );
  }

}
