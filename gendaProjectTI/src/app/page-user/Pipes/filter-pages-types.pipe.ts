import { Pipe, PipeTransform } from '@angular/core';
import {Page} from "../../model/page";

@Pipe({
  name: 'filterPagesTypes'
})
export class FilterPagesTypesPipe implements PipeTransform {

  transform(strings: Page[], filter: string): Page[] {

    const ALL_VALUES = "All";

    if(!filter || filter===ALL_VALUES)
    {
      return strings;
    }
    return strings.filter(
      value => value.type === filter
    );
  }

}
