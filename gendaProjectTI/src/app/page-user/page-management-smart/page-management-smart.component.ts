import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subscription} from "rxjs";
import {PageDto} from "../DTO/page-dto";
import {PageService} from "../Services/page.service";
import {Page} from "../../model/page";
import {ModereService} from "../Services/modere.service";
import {UtilisateurDTO} from "../../user/DTO/utilisateur-dto";
import {AppComponent} from "../../app.component";
import {ModereDTO} from "../DTO/modere-dto";

@Component({
  selector: 'app-page-management-smart',
  templateUrl: './page-management-smart.component.html',
  styleUrls: ['./page-management-smart.component.css']
})
export class PageManagementSmartComponent implements OnInit, OnDestroy {

  private subscriptions: Subscription[] = [];
  private pageSelected: PageDto;

  private moderators : UtilisateurDTO[] = [];

  constructor(public pageService : PageService,
              public modereService : ModereService) {
  }

  ngOnInit() {
    this.listenToPageSelected();
    this.getModerators();
    console.log(this.pageSelected);
  }

  ngOnDestroy(): void {
    for (let i = this.subscriptions.length - 1; i >= 0; i--) {
      const subscription = this.subscriptions[i];
      subscription && subscription.unsubscribe();
      this.subscriptions.pop();
    }
  }

  listenToPageSelected() {
    this.subscriptions.push(
      this.pageService
        .$pageSelected
        .subscribe(page => this.pageSelected = page)
    );
  }

  updatePage($event: Page) {
    this.pageService.put($event.toPageDTO()).subscribe();
  }

  deletePage($event: Page) {
    const sub = this.pageService.delete($event.id).subscribe();
  }

  getModerators() {
    this.subscriptions.push(
      this.modereService.getModerators(this.pageSelected).subscribe(
        values => this.moderators = values
      )
    );
  }

  ungrant($event: UtilisateurDTO) {
    this.subscriptions.push(
      this.modereService.ungrantModerator($event).subscribe()
    );
  }

  grantToAdmin($event: UtilisateurDTO) {
    this.subscriptions.push(
      this.modereService.grantAdministrator($event).subscribe(
        () => this.getModerators()
      )
    )
  }

  grantToModerator($event: ModereDTO) {
    console.log($event.estAdmin + " " + $event.mailUtil);
    this.subscriptions.push(
      this.modereService.grantModerator($event).subscribe(
        () => this.getModerators()
      )
    );
  }
}
