import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageManagementSmartComponent } from './page-management-smart.component';

describe('PageManagementSmartComponent', () => {
  let component: PageManagementSmartComponent;
  let fixture: ComponentFixture<PageManagementSmartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageManagementSmartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageManagementSmartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
