import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPageListSmartComponent } from './admin-page-list-smart.component';

describe('AdminPageListSmartComponent', () => {
  let component: AdminPageListSmartComponent;
  let fixture: ComponentFixture<AdminPageListSmartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminPageListSmartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPageListSmartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
