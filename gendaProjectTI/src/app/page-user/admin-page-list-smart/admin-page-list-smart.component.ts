import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {Page, PageList} from "../../model/page";
import {PageService} from "../Services/page.service";

@Component({
  selector: 'app-admin-page-list-smart',
  templateUrl: './admin-page-list-smart.component.html',
  styleUrls: ['./admin-page-list-smart.component.css']
})
export class AdminPageListSmartComponent implements OnInit, OnDestroy {

  private subscriptions: Subscription[] = [];
  private pages: PageList = [];

  constructor(public pageService : PageService) { }

  ngOnInit() {
    this.loadPages();
  }

  ngOnDestroy(): void {
    for (let i = this.subscriptions.length - 1; i >= 0; i--) {
      const subscription = this.subscriptions[i];
      subscription && subscription.unsubscribe();
      this.subscriptions.pop();
    }
  }


  // Changer le query en get toutes les pages de l'utilisateur
  loadPages(){
    this.pages.splice(0, this.pages.length);
    const sub = this.pageService
      .getModerated()
      .subscribe(pages => {
        this.pages = pages.map(pages => new Page().fromPageDTO(pages))
      });
    this.subscriptions.push(sub);
  }

}
