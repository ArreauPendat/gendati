import {Component, OnDestroy, OnInit} from '@angular/core';
import {Page, PageList} from "../../model/page";
import {PageService} from "../Services/page.service";
import {Subscription} from "rxjs";
import {PageDto} from "../DTO/page-dto";
import {LocalStorageManager} from "../../utility/local-storage-manager";

@Component({
  selector: 'app-page-list-smart',
  templateUrl: './page-list-smart.component.html',
  styleUrls: ['./page-list-smart.component.css']
})
export class PageListSmartComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  private pages: PageList = [];

  constructor(public pageService : PageService) { }

  ngOnInit() {
    this.loadPages();
  }

  ngOnDestroy(): void {
    for (let i = this.subscriptions.length - 1; i >= 0; i--) {
      const subscription = this.subscriptions[i];
      subscription && subscription.unsubscribe();
      this.subscriptions.pop();
    }
  }

  loadPages(){
    this.pages.splice(0, this.pages.length);
    const sub = this.pageService
      .getExceptUser()
      .subscribe(pages => {
        this.pages = pages.map(pages => new Page().fromPageDTO(pages))
      });
    this.subscriptions.push(sub);
  }

  followThisPage($event : PageDto) {
    $event.userToken = LocalStorageManager.getToken();
    console.log($event);
    this.subscriptions.push(
      this.pageService.follow($event).subscribe()
    );
    this.loadPages();
    this.loadPages();
  }
}
