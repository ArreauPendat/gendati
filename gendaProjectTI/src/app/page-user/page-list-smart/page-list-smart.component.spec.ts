import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageListSmartComponent } from './page-list-smart.component';

describe('PageListSmartComponent', () => {
  let component: PageListSmartComponent;
  let fixture: ComponentFixture<PageListSmartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageListSmartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageListSmartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
