import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Page} from "../../model/page";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {verifyHostBindings} from "@angular/compiler";
import {LocalStorageManager} from "../../utility/local-storage-manager";
import {Router} from "@angular/router";

@Component({
  selector: 'app-page-creation',
  templateUrl: './page-creation.component.html',
  styleUrls: ['./page-creation.component.css']
})
export class PageCreationComponent implements OnInit {

  private _page:Page;
  @Output()
  pageCreation:EventEmitter<Page> = new EventEmitter<Page>();

  form:FormGroup = this.fb.group({
    nom: this.fb.control("",Validators.required),
    type: this.fb.control("",Validators.required),
    lienSite: this.fb.control(""),
    lienTwitter: this.fb.control(""),
    lienFacebook: this.fb.control(""),
    description: this.fb.control("",Validators.required)
  })

  agreement: boolean = false;

  constructor(public fb: FormBuilder, public router:Router) { }

  ngOnInit() {
  }

  emitPage(){
    this.pageCreation.next(this.buildPage());
  }

  redirectAfterCreation(){
    this.router.navigate([("/page/admin/list")]);
  }

  private buildPage() {
    const page = new Page();
    page.id = 1;
    page.nom = this.form.get("nom").value;
    page.type = this.form.get("type").value;
    page.lienSite = this.form.get("lienSite").value;
    page.lienTwitter = this.form.get("lienTwitter").value;
    page.lienFacebook = this.form.get("lienFacebook").value;
    page.description = this.form.get("description").value;
    page.userToken = LocalStorageManager.getToken();
    return page;
  }

  updateAgreement() {
    this.agreement = !this.agreement;
  }
}
