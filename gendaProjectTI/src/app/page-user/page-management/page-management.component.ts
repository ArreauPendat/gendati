import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Page} from "../../model/page";
import {Observable} from "rxjs";
import {UtilisateurDTO} from "../../user/DTO/utilisateur-dto";
import {Utilisateur} from "../../model/utilisateur";
import {ModereDTO} from "../DTO/modere-dto";

@Component({
  selector: 'app-page-management',
  templateUrl: './page-management.component.html',
  styleUrls: ['./page-management.component.css']
})
export class PageManagementComponent implements OnInit {

  readonly FIELD_NOM:string = "nom";
  readonly FIELD_TYPE:string = "type";
  readonly FIELD_LIENSITE:string = "lienSite";
  readonly FIELD_LIENTWITTER:string = "lienTwitter";
  readonly FIELD_LIENFACEBOOK:string = "lienFacebook";
  readonly FIELD_DESCRIPTION:string = "description";

  private _moderators: UtilisateurDTO[] = [];
  get moderators(): UtilisateurDTO[] {
    return this._moderators;
  }
  @Input()
  set moderators(value: UtilisateurDTO[]) {
    this.moderators.splice(0, this.moderators.length);
    for (let i = 0; i < value.length; i++) {
      this.moderators.push(value[i]);
      if(this.moderators[i].id == 1) this.mail_current = this.moderators[i].email;
    }
  }

  private _page:Page;
  get page(): Page {
    return this._page;
  }
  @Input()
  set page(value: Page) {
    this._page = value;
  }

  @Output()
  pageChanged: EventEmitter<Page> = new EventEmitter<Page>();
  @Output()
  pageDeleted: EventEmitter<Page> = new EventEmitter<Page>();
  @Output()
  ungrantModerator: EventEmitter<UtilisateurDTO> = new EventEmitter<UtilisateurDTO>();
  @Output()
  grantAdministrator: EventEmitter<UtilisateurDTO> = new EventEmitter<UtilisateurDTO>();
  @Output()
  grantModerator: EventEmitter<ModereDTO> = new EventEmitter<ModereDTO>();

  newModeratorMail: string ="";

  form:FormGroup = this.fb.group({
    nom: this.fb.control("",Validators.required),
    type: this.fb.control("",Validators.required),
    lienSite: this.fb.control(""),
    lienTwitter: this.fb.control(""),
    lienFacebook: this.fb.control(""),
    description: this.fb.control("",Validators.required)
  })

  constructor(public fb: FormBuilder) { }

  ngOnInit() {
    this.form.get(this.FIELD_NOM).disable();
    this.form.get(this.FIELD_TYPE).disable();
    this.form.get(this.FIELD_LIENSITE).disable();
    this.form.get(this.FIELD_LIENTWITTER).disable();
    this.form.get(this.FIELD_LIENFACEBOOK).disable();
    this.form.get(this.FIELD_DESCRIPTION).disable();
    this.loadPageDetails();
  }

  private mail_current:string = "";

  private loadPageDetails() {
    this.form.get(this.FIELD_NOM).setValue(this.page.nom);
    this.form.get(this.FIELD_TYPE).setValue(this.page.type);
    this.form.get(this.FIELD_LIENSITE).setValue(this.page.lienSite);
    this.form.get(this.FIELD_LIENTWITTER).setValue(this.page.lienTwitter);
    this.form.get(this.FIELD_LIENFACEBOOK).setValue(this.page.lienFacebook);
    this.form.get(this.FIELD_DESCRIPTION).setValue(this.page.description);
  }

  activateField(field:string) {
    this.form.get(field).enable();
  }

  emitAndApplyChanges() {
    this.updatePageInfos();
    this.pageChanged.next(this.page);
    this.loadPageDetails();
  }

  emitDelete() {
    this.pageDeleted.next(this.page);
  }

  updatePageInfos() {
    this.page.nom = this.form.get(this.FIELD_NOM).value;
    this.page.type = this.form.get(this.FIELD_TYPE).value;
    this.page.lienSite = this.form.get(this.FIELD_LIENSITE).value;
    this.page.lienTwitter = this.form.get(this.FIELD_LIENTWITTER).value;
    this.page.lienFacebook = this.form.get(this.FIELD_LIENFACEBOOK).value;
    this.page.description = this.form.get(this.FIELD_DESCRIPTION).value;
  }

  ungrant(ind: number) {
    this._moderators[ind].id = this.page.id;
    this.ungrantModerator.next(this._moderators[ind]);
    this._moderators.splice(ind, 1);
  }

  grantToAdmin(ind: number) {
    if(this.mail_current === '') return;
    this.moderators[ind].id = this.page.id;
    this.grantAdministrator.next(this.moderators[ind]);
  }

  grantToModerator() {
    let modere : ModereDTO = {
      estAdmin: false,
      idPage: this.page.id,
      mailUtil : this.newModeratorMail
    }
    this.grantModerator.next(modere);
    this.newModeratorMail = "";
    console.log(this.newModeratorMail);
  }

}
