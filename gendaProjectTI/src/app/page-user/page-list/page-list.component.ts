import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Page, PageList} from "../../model/page";
import {PageService} from "../Services/page.service";
import {PageDto} from "../DTO/page-dto";

@Component({
  selector: 'app-page-list',
  templateUrl: './page-list.component.html',
  styleUrls: ['./page-list.component.css']
})
export class PageListComponent implements OnInit {

  // Select fields
  readonly OPTION_ALL:string = "All";
  readonly OPTION_ENTREPRISE:string = "Company";
  readonly OPTION_ORGANISATION:string = "Organization";
  readonly OPTION_PARTICULIER:string = "Particular";

  nameSearched:string;
  typeSearched:string = this.OPTION_ALL;

  sortByWhat: string = "asc";

  @Output()
  private followThisPage:EventEmitter<PageDto> = new EventEmitter();

  newPageFollowed(page : Page) {
    this.followThisPage.next(page.toPageDTO());
  }

  constructor(public pageService : PageService) { }

  ngOnInit() {
  }

  @Output()
  reloadPages:EventEmitter<Page> = new EventEmitter<Page>();

  emitReloadPages(){
    this.reloadPages.next();
  }

  notifyPageSelected(page: Page) {
    this.pageService.notifyPageSelected(page);
  }

  private _pages:PageList = [];

  get pages(): Page[] {
    return this._pages;
  }

  @Input()
  set pages(value: Page[]) {
    this._pages = value;
  }


}
