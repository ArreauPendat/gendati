import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageMyListComponent } from './page-my-list.component';

describe('PageMyListComponent', () => {
  let component: PageMyListComponent;
  let fixture: ComponentFixture<PageMyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageMyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageMyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
