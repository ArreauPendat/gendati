import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {PageService} from "../Services/page.service";
import {Page, PageList} from "../../model/page";
import {PageDto} from "../DTO/page-dto";

@Component({
  selector: 'app-page-my-list',
  templateUrl: './page-my-list.component.html',
  styleUrls: ['./page-my-list.component.css']
})
export class PageMyListComponent implements OnInit {


  // Select fields
  readonly OPTION_ALL:string = "All";
  readonly OPTION_ENTREPRISE:string = "Company";
  readonly OPTION_ORGANISATION:string = "Organization";
  readonly OPTION_PARTICULIER:string = "Particular";

  nameSearched:string;
  typeSearched:string = this.OPTION_ALL;

  @Output()
  private unfollowPage:EventEmitter<PageDto> = new EventEmitter<PageDto>();

  @Output()
  reloadPages:EventEmitter<Page> = new EventEmitter<Page>();

  emitReloadPages(){
    this.reloadPages.next();
  }

  sortByWhat: string = "asc";

  constructor(public pageService : PageService) { }

  ngOnInit() {
  }

  notifyPageSelected(page: Page) {
    this.pageService.notifyPageSelected(page);
  }

  private _pages:PageList = [];

  get pages(): Page[] {
    return this._pages;
  }

  @Input()
  set pages(value: Page[]) {
    this._pages = value;
  }

  notifyUnfollow(i: number) {
    this.unfollowPage.next(this._pages[i]);
    this._pages.splice(i, 1);
  }
}
