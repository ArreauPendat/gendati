import {Component, OnDestroy, OnInit} from '@angular/core';
import {Page} from "../../model/page";
import {Subscription} from "rxjs";
import {PageService} from "../Services/page.service";

@Component({
  selector: 'app-page-creation-smart',
  templateUrl: './page-creation-smart.component.html',
  styleUrls: ['./page-creation-smart.component.css']
})
export class PageCreationSmartComponent implements OnInit, OnDestroy {

  private subscriptions: Subscription[] = [];

  constructor(public pageService : PageService) { }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    for (let i = this.subscriptions.length - 1; i >= 0; i--) {
      const subscription = this.subscriptions[i];
      subscription && subscription.unsubscribe();
      this.subscriptions.pop();
    }
  }

  createPage($event: Page){
    const sub = this.pageService.post($event.toPageDTO()).subscribe();
    this.subscriptions.push(sub);
  }
}
