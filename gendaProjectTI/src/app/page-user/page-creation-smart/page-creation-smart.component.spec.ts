import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageCreationSmartComponent } from './page-creation-smart.component';

describe('PageCreationSmartComponent', () => {
  let component: PageCreationSmartComponent;
  let fixture: ComponentFixture<PageCreationSmartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageCreationSmartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageCreationSmartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
