export interface PageDto {
  id:number;
  nom:string;
  type:string;
  lienSite:string;
  lienTwitter:string;
  lienFacebook:string;
  description:string;
  userToken?:string;
}
