import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageMyListSmartComponent } from './page-my-list-smart.component';

describe('PageMyListSmartComponent', () => {
  let component: PageMyListSmartComponent;
  let fixture: ComponentFixture<PageMyListSmartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageMyListSmartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageMyListSmartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
