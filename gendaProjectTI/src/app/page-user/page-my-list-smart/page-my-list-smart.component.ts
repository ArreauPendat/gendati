import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {Page, PageList} from "../../model/page";
import {PageService} from "../Services/page.service";
import {PageDto} from "../DTO/page-dto";

@Component({
  selector: 'app-page-my-list-smart',
  templateUrl: './page-my-list-smart.component.html',
  styleUrls: ['./page-my-list-smart.component.css']
})
export class PageMyListSmartComponent implements OnInit,OnDestroy {

  private subscriptions: Subscription[] = [];
  private pages: PageList = [];

  constructor(public pageService : PageService) { }

  ngOnInit() {
    this.loadPages();
  }

  ngOnDestroy(): void {
    for (let i = this.subscriptions.length - 1; i >= 0; i--) {
      const subscription = this.subscriptions[i];
      subscription && subscription.unsubscribe();
      this.subscriptions.pop();
    }
  }

  loadPages(){
    this.pages.splice(0, this.pages.length);
    const sub = this.pageService
      .getFromUser()
      .subscribe(pages => {
        this.pages = pages.map(pages => new Page().fromPageDTO(pages))
      });
    this.subscriptions.push(sub);
  }

  unFollow($event: PageDto) {
    this.subscriptions.push(
      this.pageService
        .unfollow($event)
        .subscribe(
          () => {
            this.pages.splice(0, this.pages.length);
            this.loadPages();
          }
        )
    )
  }

}
