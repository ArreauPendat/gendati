import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable, ReplaySubject, Subject} from "rxjs";
import {PageDto} from "../DTO/page-dto";
import {Page} from "../../model/page";
import {LocalStorageManager} from "../../utility/local-storage-manager";
import {Local} from "protractor/built/driverProviders";

const URL_API:string = "/api/page";
const FOLLOW_URL_API:string = "/api/suit";

@Injectable({
  providedIn: 'root'
})
export class PageService {

  constructor(public http:HttpClient) { }

  private readonly  pageSelected:ReplaySubject<PageDto> = new ReplaySubject();
  public $pageSelected : Observable<PageDto> = this.pageSelected.asObservable();

  query():Observable<PageDto[]>{
    return this.http.get<PageDto[]>(URL_API);
  }

  get(id : number):Observable<PageDto>{
    return this.http.get<PageDto>(URL_API);
  }

  post(page: PageDto):Observable<PageDto>{
    return this.http.post<PageDto>(URL_API,page);
  }

  delete(id : number):Observable<any>{
    return this.http.delete(URL_API + '/' + id);
  }

  put(page : PageDto):Observable<any>{
    return this.http.put(URL_API,page);
  }

  notifyPageSelected(page : PageDto) : void {
    this.pageSelected.next(page);
    console.log("Service : "+page);
  }

  follow(page : PageDto) : Observable<PageDto> {
    return this.http.post<PageDto>(FOLLOW_URL_API, page);
  }

  unfollow(page : PageDto) : any {
    return this.http.delete(FOLLOW_URL_API + "/" + page.id + "&" + LocalStorageManager.getToken());
  }

  // PAGES THAT HE DOESNT FOLLOW
  getExceptUser() : Observable<PageDto[]> {
    return this.http.get<PageDto[]>(URL_API+"/1&" + LocalStorageManager.getToken());
  }

  // PAGES THAT HE FOLLOWS
  getFromUser() : Observable<PageDto[]> {
    return this.http.get<PageDto[]>(URL_API+"/2&" + LocalStorageManager.getToken());
  }

  // PAGES THAT HE MODERATES
  getModerated() : Observable<PageDto[]> {
    return this.http.get<PageDto[]>(URL_API + "/3&" + LocalStorageManager.getToken());
  }

}
