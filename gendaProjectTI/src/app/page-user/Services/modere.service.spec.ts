import { TestBed } from '@angular/core/testing';

import { ModereService } from './modere.service';

describe('ModereService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ModereService = TestBed.get(ModereService);
    expect(service).toBeTruthy();
  });
});
