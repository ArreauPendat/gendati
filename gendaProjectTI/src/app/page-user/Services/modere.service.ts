import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {UtilisateurDTO} from "../../user/DTO/utilisateur-dto";
import {PageDto} from "../DTO/page-dto";
import {LocalStorageManager} from "../../utility/local-storage-manager";
import {ModereDTO} from "../DTO/modere-dto";

@Injectable({
  providedIn: 'root'
})
export class ModereService {

  private readonly URL_API : string = "/api/modere";

  constructor(public http:HttpClient) { }

  getModerators(page: PageDto) : Observable<UtilisateurDTO[]> {
    return this.http.get<UtilisateurDTO[]>(this.URL_API + "/" + page.id + "&" + LocalStorageManager.getToken());
  }

  ungrantModerator(user : UtilisateurDTO) {
    //user id must be set to the page ID to delete entry in modere table
    return this.http.delete<any>(this.URL_API + "/" + user.id + "&" + user.token);
  }

  grantAdministrator(user: UtilisateurDTO) {
    return this.http.put<any>(this.URL_API + "/" + user.id, user);
  }

  grantModerator(modere: ModereDTO) {
    console.log(modere.mailUtil);
    return this.http.post<any>(this.URL_API, modere);
  }



}
