import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Utilisateur} from "../../model/utilisateur";
import {Router} from "@angular/router";

@Component({
  selector: 'app-creation-utilisateur',
  templateUrl: './creation-utilisateur.component.html',
  styleUrls: ['./creation-utilisateur.component.css']
})
export class CreationUtilisateurComponent implements OnInit {
  private creationCompte: FormGroup = this.fb.group({
    email: this.fb.control("", Validators.required),
    nom : this.fb.control("", Validators.required && Validators.maxLength(50) && Validators.minLength(1)),
    prenom : this.fb.control("", Validators.required && Validators.maxLength(50) && Validators.minLength(1)),
    password : this.fb.control("", Validators.required && Validators.minLength(8))
  });

  readonly INPUT_DEFAULTSTYLE: string = "form-control";
  readonly BUTTON_DEFAULTSTYLE: string = "btn btn-warning mt-5";
  readonly DEFAULT_DIVPARAMETERS: string = "form-row mt-3";

  @Output()
  private eventCreationUtilisateur : EventEmitter<Utilisateur> = new EventEmitter<Utilisateur>();

  constructor(public fb : FormBuilder, public router:Router) { }

  ngOnInit() {

  }

  creationUtilisateur() : Utilisateur {
    let util:Utilisateur = new Utilisateur();
    util.email = this.creationCompte.get('email').value;
    util.nom = this.creationCompte.get('nom').value;
    util.prenom = this.creationCompte.get('prenom').value;
    util.estAdmin = false;
    util.password = this.creationCompte.get('password').value;
    return util;
  }

  notifyCreationUtilisateur() {
    this.eventCreationUtilisateur.next(this.creationUtilisateur());
  }
}
