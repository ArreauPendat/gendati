import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreationSmartComponent } from './creation-smart.component';

describe('CreationSmartComponent', () => {
  let component: CreationSmartComponent;
  let fixture: ComponentFixture<CreationSmartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreationSmartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreationSmartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
