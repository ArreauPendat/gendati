import {Component, OnDestroy, OnInit} from '@angular/core';
import {Utilisateur} from "../../../model/utilisateur";
import {LocalStorageManager} from "../../../utility/local-storage-manager";
import {UtilisateurService} from "../../Services/utilisateur.service";
import {Subscription} from "rxjs";
import {UtilisateurConnectedService} from "../../Services/utilisateur-connected.service";
import {UtilisateurDTO} from "../../DTO/utilisateur-dto";
import {Router} from "@angular/router";

@Component({
  selector: 'app-creation-smart',
  templateUrl: './creation-smart.component.html',
  styleUrls: ['./creation-smart.component.css']
})
export class CreationSmartComponent implements OnInit, OnDestroy {

  private subscriptions:Subscription[] = [];
  private user : UtilisateurDTO = null;

  constructor(public utilisateurService : UtilisateurService,
              public utilisateurConnected : UtilisateurConnectedService,
              public router : Router) { }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    for (let i = this.subscriptions.length - 1; i >= 0; i--) {
      const subscription = this.subscriptions[i];
      subscription && subscription.unsubscribe();
      this.subscriptions.pop();
    }
  }

  //TODO rediriger l'utilisateur sur le dashboard après création
  createAndConnectNewUser($event: Utilisateur) {
    this.subscriptions.push(
      this.utilisateurService.post($event.toUtilisateurDTO()).subscribe(
        answer => {
          this.user = answer;
        }
      )
    );
    if(this.user != null) {
      this.notifyUserIsConnected();
    }
  }

  private notifyUserIsConnected() : void {
    this.utilisateurConnected.notifyConnexion(this.user);
  }

}

