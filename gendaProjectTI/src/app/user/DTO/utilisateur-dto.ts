export interface UtilisateurDTO {

  id?:number;
  nom:string;
  prenom:string;
  estAdmin?:boolean;
  password:string;
  email:string;
  token?:string;

}
