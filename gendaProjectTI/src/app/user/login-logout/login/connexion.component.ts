import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UtilisateurConnectedService} from "../../Services/utilisateur-connected.service";
import {UtilisateurDTO} from "../../DTO/utilisateur-dto";
import {HashSystemService} from "../../../utility/hash-system.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.css']
})
export class ConnexionComponent implements OnInit {

  readonly INPUT_DEFAULTCLASS:string = "form-control m-2";
  readonly BUTTON_DEFAULTCLASS: string = "btn btn-primary m-2";
  readonly LABEL_DEFAULTCLASS: string = "h5 m-2";

  connexion: FormGroup = this.fb.group({
    login : this.fb.control("", Validators.required && Validators.maxLength(50)),
    password : this.fb.control("", Validators.required)
  });

  @Output()
  private emitConnection : EventEmitter<UtilisateurDTO> = new EventEmitter<UtilisateurDTO>();

  @Input()
  private isValid : boolean;

  constructor(public fb: FormBuilder,public router:Router) {}

  ngOnInit() {}

  notifyConnexion() {
//    this.utilisateurConnectedService.notifyConnexion(this.buildConnexion());
    this.isValid = true;
    this.emitConnection.next(this.buildConnexion());
  }

  buildConnexion() : UtilisateurDTO {
    return {
      email: this.connexion.get('login').value,
      password: this.connexion.get('password').value,
      nom: "",
      prenom: ""
    }
  }


}
