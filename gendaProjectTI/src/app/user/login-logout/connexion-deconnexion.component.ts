import {Component, OnDestroy, OnInit} from '@angular/core';
import {UtilisateurDTO} from "../DTO/utilisateur-dto";
import {UtilisateurConnectedService} from "../Services/utilisateur-connected.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-connexion-deconnexion',
  templateUrl: './connexion-deconnexion.component.html',
  styleUrls: ['./connexion-deconnexion.component.css']
})
export class ConnexionDeconnexionComponent implements OnInit, OnDestroy {

  private userConnected : UtilisateurDTO = null;
  private subscriptions : Subscription[] = [];
  isValid: boolean = true;
  //style="background-image: url('../../../assets/images/agenda_side.jpg'); background-repeat: repeat; background-size: 100%"
  constructor(public utilisateurConnectedService : UtilisateurConnectedService) { }

  ngOnInit() {
    this.listenToConnexionProblem();
  }

  ngOnDestroy(): void {
    for (let i = this.subscriptions.length - 1; i >= 0; i--) {
      const subscription = this.subscriptions[i];
      subscription && subscription.unsubscribe();
      this.subscriptions.pop();
    }
  }

  notifyConnection($event: UtilisateurDTO) {
    this.utilisateurConnectedService.notifyConnexion($event);
    this.userConnected = $event;
  }

  private listenToConnexionProblem() {
    this.subscriptions.push(
      this.utilisateurConnectedService.$connectionProblem.subscribe(() =>
        {
          this.isValid = false;
        }
      )
    );
  }
}
