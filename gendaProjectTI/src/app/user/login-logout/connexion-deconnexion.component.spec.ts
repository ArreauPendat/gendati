import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnexionDeconnexionComponent } from './connexion-deconnexion.component';

describe('ConnexionDeconnexionComponent', () => {
  let component: ConnexionDeconnexionComponent;
  let fixture: ComponentFixture<ConnexionDeconnexionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConnexionDeconnexionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnexionDeconnexionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
