import { Component, OnInit } from '@angular/core';
import {UtilisateurDTO} from "../../DTO/utilisateur-dto";
import {UtilisateurService} from "../../Services/utilisateur.service";
import {Subscription} from "rxjs";
import {LocalStorageManager} from "../../../utility/local-storage-manager";

@Component({
  selector: 'app-settings-smart',
  templateUrl: './settings-smart.component.html',
  styleUrls: ['./settings-smart.component.css']
})
export class SettingsSmartComponent implements OnInit {

  user: UtilisateurDTO = null;
  private subscriptions : Subscription[] = [];

  constructor(public utilisateurService : UtilisateurService) { }

  ngOnInit() {
    this.getUserByToken();
  }

  private getUserByToken() {
    let token:string = LocalStorageManager.getToken();
    if(token === "" || token == null) return;
    this.subscriptions.push(
      this.utilisateurService.connectWithToken(token).subscribe(
        answer => this.user = answer
      )
    );
  }

}
