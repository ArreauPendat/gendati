import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingsSmartComponent } from './settings-smart.component';

describe('SettingsSmartComponent', () => {
  let component: SettingsSmartComponent;
  let fixture: ComponentFixture<SettingsSmartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingsSmartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsSmartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
