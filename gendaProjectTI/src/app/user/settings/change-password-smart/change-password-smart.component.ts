import {Component, OnDestroy, OnInit} from '@angular/core';
import {UtilisateurService} from "../../Services/utilisateur.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-change-password-smart',
  templateUrl: './change-password-smart.component.html',
  styleUrls: ['./change-password-smart.component.css']
})
export class ChangePasswordSmartComponent implements OnInit {

  constructor(public utilisateurService : UtilisateurService) { }

  ngOnInit() {
  }

  changePassword($event: string) {
    console.log("change : " + $event);
    this.utilisateurService.notifyChangePassword($event);
  }

}
