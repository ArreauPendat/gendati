import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangePasswordSmartComponent } from './change-password-smart.component';

describe('ChangePasswordSmartComponent', () => {
  let component: ChangePasswordSmartComponent;
  let fixture: ComponentFixture<ChangePasswordSmartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangePasswordSmartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangePasswordSmartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
