import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-change-mail',
  templateUrl: './change-mail.component.html',
  styleUrls: ['./change-mail.component.css']
})
export class ChangeMailComponent implements OnInit {

  form: FormGroup = this.fb.group({
    newMail : this.fb.control("", Validators.required)
  });

  @Output()
  private changedMail : EventEmitter<string> = new EventEmitter();

  constructor(public fb : FormBuilder) { }

  ngOnInit() {
  }

  changeMail() {
    this.changedMail.next(this.form.get("newMail").value);
    this.form.reset();
  }

  isMailValid() {
    return this.form.get("newMail").value.toString().trim() !== "" || this.form.get("newMail").value != null;
  }
}
