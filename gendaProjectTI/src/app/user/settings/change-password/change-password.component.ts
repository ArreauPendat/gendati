import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {

  form: FormGroup = this.fb.group({
    firstPassword : this.fb.control("", Validators.required),
    confirmPassword : this.fb.control("", Validators.required)
  });

  @Output()
  private changedPassword : EventEmitter<string> = new EventEmitter();

  constructor(public fb : FormBuilder) { }

  ngOnInit() {
  }

  changePassword() {
    if(this.isPasswordSame()) {return;}
    this.changedPassword.next(this.form.get("confirmPassword").value);
    this.form.reset();
  }

  isPasswordSame() : boolean {
    return this.form.get("firstPassword").value !== this.form.get("confirmPassword").value
      || this.form.get("firstPassword") == null || this.form.get("firstPassword").value.toString().trim() === "";
  }

}
