import { Component, OnInit } from '@angular/core';
import {UtilisateurService} from "../../Services/utilisateur.service";

@Component({
  selector: 'app-change-mail-smart',
  templateUrl: './change-mail-smart.component.html',
  styleUrls: ['./change-mail-smart.component.css']
})
export class ChangeMailSmartComponent implements OnInit {

  constructor(public utilisateurService : UtilisateurService) { }

  ngOnInit() {
  }

  changeMail($event: string) {
    this.utilisateurService.notifyChangeMail($event);
  }
}
