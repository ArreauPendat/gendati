import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeMailSmartComponent } from './change-mail-smart.component';

describe('ChangeMailSmartComponent', () => {
  let component: ChangeMailSmartComponent;
  let fixture: ComponentFixture<ChangeMailSmartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeMailSmartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeMailSmartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
