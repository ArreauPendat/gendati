import {Component, Input, OnInit} from '@angular/core';
import {UtilisateurDTO} from "../../DTO/utilisateur-dto";

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  constructor() { }

  @Input()
  private userSelected : UtilisateurDTO = null;

  ngOnInit() {
  }

}
