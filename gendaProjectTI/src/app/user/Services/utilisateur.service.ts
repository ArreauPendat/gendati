import {EventEmitter, Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable, Subject} from "rxjs";
import {Utilisateur} from "../../model/utilisateur";
import {UtilisateurDTO} from "../DTO/utilisateur-dto";

@Injectable({
  providedIn: 'root'
})
export class UtilisateurService {

  private readonly HTTP_URL:string = "/api/utilisateur"

  private readonly changePassword:Subject<string> = new Subject();
  public $changePassword : Observable<string> = this.changePassword.asObservable();
  private readonly changeMail:Subject<string> = new Subject();
  public $changeMail : Observable<string> = this.changeMail.asObservable();

  constructor(public http : HttpClient) { }

  getConnexion(utilisateur : UtilisateurDTO) : Observable<UtilisateurDTO> {
    return this.http.get<UtilisateurDTO>( `${this.HTTP_URL}/${utilisateur.email}&${utilisateur.password}`);
  }

  post(utilisateur : UtilisateurDTO) : Observable<UtilisateurDTO> {
    return this.http.post<Utilisateur>(this.HTTP_URL, utilisateur);
  }

  connectWithToken(tokenString : string) : Observable<UtilisateurDTO> {
    return this.http.get<UtilisateurDTO>(this.HTTP_URL+"/"+tokenString);
  }

  put(utilisateur : UtilisateurDTO) : Observable<UtilisateurDTO> {
    return this.http.put<UtilisateurDTO>(this.HTTP_URL, utilisateur);
  }

  notifyChangePassword(value : string) {
    this.changePassword.next(value);
  }

  notifyChangeMail(value : string) {
    this.changeMail.next(value);
  }

}
