import { TestBed } from '@angular/core/testing';

import { UtilisateurConnectedService } from './utilisateur-connected.service';

describe('UtilisateurConnectedService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UtilisateurConnectedService = TestBed.get(UtilisateurConnectedService);
    expect(service).toBeTruthy();
  });
});
