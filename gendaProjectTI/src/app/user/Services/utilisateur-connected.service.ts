import { Injectable } from '@angular/core';
import {Observable, Subject} from "rxjs";
import {UtilisateurDTO} from "../DTO/utilisateur-dto";



@Injectable({
  providedIn: 'root'
})
export class UtilisateurConnectedService {

  private readonly  subject:Subject<UtilisateurDTO> = new Subject();
  public $utilisateurConnected : Observable<UtilisateurDTO> = this.subject.asObservable();
  private readonly subjectConnection:Subject<number> = new Subject<number>();
  public readonly  $connectionProblem: Observable<number> = this.subjectConnection.asObservable();

  constructor() { }

  notifyConnexion(utilisateur : UtilisateurDTO) : void {
    this.subject.next(utilisateur);
  }

  notifyProblem(value : number) : void {
    this.subjectConnection.next(value);
  }

}
