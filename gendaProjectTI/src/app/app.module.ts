import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import { ScheduleModule, RecurrenceEditorModule, DayService, WeekService,WorkWeekService,MonthService,MonthAgendaService } from '@syncfusion/ej2-angular-schedule';
import {NavigationBarComponent} from "./structural-components/navigation-bar/navigation-bar.component";
import {CreationEvenementComponent} from "./event/creation-event/creation-evenement.component";
import {CalendarComponent} from "./event/calendar/calendar.component";
import {ConnexionDeconnexionComponent} from "./user/login-logout/connexion-deconnexion.component";
import {ConnexionComponent} from "./user/login-logout/login/connexion.component";
import {CreationUtilisateurComponent} from "./user/creation-user/creation-utilisateur.component";
import {DeconnexionComponent} from "./user/login-logout/logout/deconnexion.component";
import {PageCreationComponent} from "./page-user/page-creation/page-creation.component";
import {PageCreationSmartComponent} from "./page-user/page-creation-smart/page-creation-smart.component";
import { ChangeEventComponent } from './event/change-event/change-event.component';
import { PageDisplayComponent } from './page-user/page-display/page-display.component';
import { PageListComponent } from './page-user/page-list/page-list.component';
import {RouterModule, Routes} from "@angular/router";
import { FourNulFourErrorComponent } from './structural-components/four-nul-four-error/four-nul-four-error.component';
import { DashboardSmartComponent } from './structural-components/dashboard/dashboard-smart/dashboard-smart.component';
import { CreationSmartComponent } from './user/creation-user/creation-smart/creation-smart.component';
import { PageManagementComponent } from './page-user/page-management/page-management.component';
import { CalendarSmartComponent } from './event/calendar-smart/calendar-smart.component';
import { ChangeEventSmartComponent } from './event/change-event-smart/change-event-smart.component';
import { CreationEventSmartComponent } from './event/creation-event-smart/creation-event-smart.component';
import {PageListSmartComponent} from "./page-user/page-list-smart/page-list-smart.component";
import {PageDisplaySmartComponent} from "./page-user/page-display-smart/page-display-smart.component";
import { PageManagementSmartComponent } from './page-user/page-management-smart/page-management-smart.component';
import { AdminPageListComponent } from './page-user/admin-page-list/admin-page-list.component';
import { AdminPageListSmartComponent } from './page-user/admin-page-list-smart/admin-page-list-smart.component';
import { SettingsComponent } from './user/settings/settings/settings.component';
import { SettingsSmartComponent } from './user/settings/settings-smart/settings-smart.component';
import { ChangePasswordComponent } from './user/settings/change-password/change-password.component';
import { ChangePasswordSmartComponent } from './user/settings/change-password-smart/change-password-smart.component';
import { ChangeMailComponent } from './user/settings/change-mail/change-mail.component';
import { ChangeMailSmartComponent } from './user/settings/change-mail-smart/change-mail-smart.component';
import { FilterPagesNamesPipe } from './page-user/Pipes/filter-pages-names.pipe';
import { FilterPagesTypesPipe } from './page-user/Pipes/filter-pages-types.pipe';
import { EventListComponent } from './event/event-list/event-list.component';
import { EventListSmartComponent } from './event/event-list-smart/event-list-smart.component';
import { EventDisplayComponent } from './event/event-display/event-display.component';
import { EventDisplaySmartComponent } from './event/event-display-smart/event-display-smart.component';
import { FilterEventsNamesPipe } from './event/Pipes/filter-events-names.pipe';
import { SortByPipe } from './utility/sort-by.pipe';
import { EventMyListComponent } from './event/event-my-list/event-my-list.component';
import { EventMyListSmartComponent } from './event/event-my-list-smart/event-my-list-smart.component';
import { PageMyListComponent } from './page-user/page-my-list/page-my-list.component';
import { PageMyListSmartComponent } from './page-user/page-my-list-smart/page-my-list-smart.component';
import { FaqComponent } from './structural-components/faq/faq.component';
import {TrendsSmartComponent} from './event/trends-smart/trends-smart.component';
import {TrendsComponent} from './event/trends/trends.component';
import {ConnectedGuardService} from "./utility/connected-guard.service";
import {BlockLoginpageService} from "./utility/block-loginpage.service";

/************ ROUTING *************/
const routes:Routes = [
  { path:'', canActivate:[ConnectedGuardService],component:PageListSmartComponent },
  { path:"log/in", canActivate:[BlockLoginpageService],component:ConnexionDeconnexionComponent },
  { path:"account/create", canActivate:[BlockLoginpageService],component:CreationSmartComponent },
  { path:"page/list", canActivate:[ConnectedGuardService],component:PageListSmartComponent },
  { path:"page/create", canActivate:[ConnectedGuardService],component:PageCreationSmartComponent },
  { path:"page/detail", canActivate:[ConnectedGuardService],component:PageDisplaySmartComponent },
  { path:"page/management", canActivate:[ConnectedGuardService],component:PageManagementSmartComponent },
  { path:"page/admin/list", canActivate:[ConnectedGuardService],component:AdminPageListSmartComponent },
  { path:"page/mylist", canActivate:[ConnectedGuardService],component:PageMyListSmartComponent },
  { path:"calendar", canActivate:[ConnectedGuardService],component:CalendarSmartComponent },
  { path:"event/create", canActivate:[ConnectedGuardService],component:CreationEventSmartComponent },
  { path:"event/management", canActivate:[ConnectedGuardService],component:ChangeEventSmartComponent },
  { path:"event/list", canActivate:[ConnectedGuardService],component:EventListSmartComponent },
  { path:"event/detail", canActivate:[ConnectedGuardService],component:EventDisplaySmartComponent },
  { path:"event/mylist", canActivate:[ConnectedGuardService],component:EventMyListSmartComponent },
  { path:"dashboard", canActivate:[ConnectedGuardService],component:DashboardSmartComponent },
  { path:"account/settings", canActivate:[ConnectedGuardService],component:SettingsSmartComponent},
  { path:"account/change/password", canActivate:[ConnectedGuardService],component: ChangePasswordSmartComponent},
  { path:"account/change/mail", canActivate:[ConnectedGuardService],component: ChangeMailSmartComponent},
  { path:"faq",component: FaqComponent},
  { path:"trends", canActivate:[ConnectedGuardService],component: TrendsSmartComponent},
  // 404 Error
  { path:"not-found", canActivate:[ConnectedGuardService],component: FourNulFourErrorComponent},
  { path:"**", canActivate:[ConnectedGuardService],redirectTo:"/not-found"}
];

@NgModule({
  declarations: [
    AppComponent,
    NavigationBarComponent,
    ConnexionDeconnexionComponent,
    ConnexionComponent,
    DeconnexionComponent,
    PageCreationComponent,
    CreationEvenementComponent,
    CreationUtilisateurComponent,
    CalendarComponent,
    ChangeEventComponent,
    PageDisplayComponent,
    PageListComponent,
    FourNulFourErrorComponent,
    DashboardSmartComponent,
    CreationSmartComponent,
    PageManagementComponent,
    CalendarSmartComponent,
    ChangeEventSmartComponent,
    CreationEventSmartComponent,
    PageListSmartComponent,
    PageDisplaySmartComponent,
    PageCreationSmartComponent,
    PageManagementSmartComponent,
    AdminPageListComponent,
    AdminPageListSmartComponent,
    SettingsComponent,
    SettingsSmartComponent,
    ChangePasswordComponent,
    ChangePasswordSmartComponent,
    ChangeMailComponent,
    ChangeMailSmartComponent,
    FilterPagesNamesPipe,
    FilterPagesTypesPipe,
    EventListComponent,
    EventListSmartComponent,
    EventDisplayComponent,
    EventDisplaySmartComponent,
    FilterEventsNamesPipe,
    SortByPipe,
    EventMyListComponent,
    EventMyListSmartComponent,
    TrendsComponent,
    TrendsSmartComponent,
    PageMyListComponent,
    PageMyListSmartComponent,
    FaqComponent,
    PageMyListSmartComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ScheduleModule,
    RecurrenceEditorModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes)
  ],
  providers: [DayService, WeekService,WorkWeekService,MonthService,MonthAgendaService,ConnectedGuardService,BlockLoginpageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
