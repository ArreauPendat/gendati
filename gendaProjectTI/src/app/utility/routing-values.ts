export abstract class RoutingValues {

  // GLOBAL
  public static readonly ALL_PAGES_ROUTING:string = "/page/list";
  public static readonly ALL_EVENTS_ROUTING:string = "/event/list";
  public static readonly TRENDS_ROUTING:string = "/trends";

  // FEED
  public static readonly MY_PAGES_ROUTING:string = "/page/mylist";
  public static readonly MY_EVENTS_ROUTING:string = "/event/mylist";
  public static readonly MY_GENDA_ROUTING:string = "/calendar";

  // MANAGEMENT
  public static readonly MANAGEMENT_MY_PAGES_ROUTING:string = "/page/admin/list";
  public static readonly MANAGEMENT_MY_EVENTS_ROUTING:string = "/event/management";
  public static readonly MANAGEMENT_NEW_PAGE_ROUTING:string = "/page/create";

}
