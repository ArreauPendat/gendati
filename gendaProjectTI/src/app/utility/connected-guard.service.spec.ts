import { TestBed } from '@angular/core/testing';

import { ConnectedGuardService } from './connected-guard.service';

describe('ConnectedGuardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ConnectedGuardService = TestBed.get(ConnectedGuardService);
    expect(service).toBeTruthy();
  });
});
