import { TestBed } from '@angular/core/testing';

import { HashSystemService } from './hash-system.service';

describe('HashSystemService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HashSystemService = TestBed.get(HashSystemService);
    expect(service).toBeTruthy();
  });
});
