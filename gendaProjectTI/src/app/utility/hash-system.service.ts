import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';

@Injectable({
  providedIn: 'root'
})
export class HashSystemService {

  private readonly SECRET_KEY: string = "This is the super secret key in Angular, I admit it is awesome";

  constructor() { }

  encrypt(value : string) : string {
    return CryptoJS.AES.encrypt(value, this.SECRET_KEY.trim()).toString();
  }

}
