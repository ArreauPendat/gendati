import { Injectable } from '@angular/core';
import {LocalStorageManager} from "./local-storage-manager";
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ConnectedGuardService implements CanActivate {

  userToken:string;

  constructor(private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot,state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    this.userToken = LocalStorageManager.getToken();
    if(this.userToken!=null) {
      return true;
    } else {
      this.router.navigate(['log/in']);
    }
  }
}
