import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";
import {Observable} from "rxjs";
import {LocalStorageManager} from "./local-storage-manager";

@Injectable({
  providedIn: 'root'
})
export class BlockLoginpageService implements CanActivate{

  userToken:string;

  constructor(private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot,state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    this.userToken = LocalStorageManager.getToken();
    if(this.userToken!=null) {
      this.router.navigate(['page/list']);
    } else {
      return true;
    }
  }
}
