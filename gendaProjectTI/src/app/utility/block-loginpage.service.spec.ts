import { TestBed } from '@angular/core/testing';

import { BlockLoginpageService } from './block-loginpage.service';

describe('BlockLoginpageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BlockLoginpageService = TestBed.get(BlockLoginpageService);
    expect(service).toBeTruthy();
  });
});
