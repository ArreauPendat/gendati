export class LocalStorageManager {

  public static readonly TOKEN_KEY : string = "GendaToken";

  public static getToken():string {
    return localStorage.getItem(this.TOKEN_KEY);
  }

  public static setToken(value : string):string {
    localStorage.setItem(this.TOKEN_KEY, value);
    return value;
  }

  public static resetToken():void {
    localStorage.removeItem(this.TOKEN_KEY);
  }

}
