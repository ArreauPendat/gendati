import {UtilisateurDTO} from "../user/DTO/utilisateur-dto";

export class Utilisateur {

  private _id:number;
  private _nom:string;
  private _prenom:string;
  private _estAdmin:boolean;
  private _password:string;
  private _email:string;


  constructor() {}

  public toUtilisateurDTO() : UtilisateurDTO {
    return {
      id:this._id,
      nom:this._nom,
      prenom:this._prenom,
      estAdmin:this._estAdmin,
      password:this._password,
      email:this._email
    };
  }


  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get nom(): string {
    return this._nom;
  }

  set nom(value: string) {
    this._nom = value;
  }

  get prenom(): string {
    return this._prenom;
  }

  set prenom(value: string) {
    this._prenom = value;
  }

  get estAdmin(): boolean {
    return this._estAdmin;
  }

  set estAdmin(value: boolean) {
    this._estAdmin = value;
  }

  get password(): string {
    return this._password;
  }

  set password(value: string) {
    this._password = value;
  }

  get email(): string {
    return this._email;
  }

  set email(value: string) {
    this._email = value;
  }
}
