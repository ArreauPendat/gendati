import {EvenementDTO} from "../event/DTO/EvenementDTO";
import {Time} from "@angular/common";
import DateTimeFormat = Intl.DateTimeFormat;

export declare type EventList = Event[];

export class Event{
  get userToken(): string {
    return this._userToken;
  }

  set userToken(value: string) {
    this._userToken = value;
  }

  private _idEvent:number;
  private _nomEvent:string;
  private _nbPersMinEvent:number;
  private _nbPersMaxEvent:number;
  private _descriptionEvent:string;
  private _idOrga:number;
  private _dateDebEvent:Date;
  private _dateFinEvent:Date;
  private _userToken:string;

  constructor(idEvent: number = 1, nomEvent: string="", nbPersMinEvent: number=0, nbPersMaxEvent: number=1, descriptionEvent: string="", idOrga: number=-1, dateDebEvent: Date=new Date(), dateFinEvent: Date=new Date()) {
    this._idEvent = idEvent;
    this._nomEvent = nomEvent;
    this._nbPersMinEvent = nbPersMinEvent;
    this._nbPersMaxEvent = nbPersMaxEvent;
    this._descriptionEvent = descriptionEvent;
    this._idOrga = idOrga;
    this._dateDebEvent = dateDebEvent;
    this._dateFinEvent = dateFinEvent;
  }

  fromEvenementDTO(dto:EvenementDTO): Event{
    Object.assign(this, dto);
    return this;
  }

  toEvenementDTO():EvenementDTO{
    return{
      idEvent:this._idEvent,
      nomEvent:this._nomEvent,
      nbPersMinEvent:this._nbPersMinEvent,
      nbPersMaxEvent:this._nbPersMaxEvent,
      descriptionEvent:this._descriptionEvent,
      idOrga:this._idOrga,
      dateDebEvent:this._dateDebEvent,
      dateFinEvent:this._dateFinEvent,
      userToken:this._userToken
    };
  }

  equals(obj):boolean{
    if(obj instanceof Event){
      return this._idEvent === obj._idEvent;
    }
    return false;
  }


  get idEvent(): number {
    return this._idEvent;
  }

  set idEvent(value: number) {
    this._idEvent = value;
  }

  get nomEvent(): string {
    return this._nomEvent;
  }

  set nomEvent(value: string) {
    this._nomEvent = value;
  }

  get nbPersMinEvent(): number {
    return this._nbPersMinEvent;
  }

  set nbPersMinEvent(value: number) {
    this._nbPersMinEvent = value;
  }

  get nbPersMaxEvent(): number {
    return this._nbPersMaxEvent;
  }

  set nbPersMaxEvent(value: number) {
    this._nbPersMaxEvent = value;
  }

  get descriptionEvent(): string {
    return this._descriptionEvent;
  }

  set descriptionEvent(value: string) {
    this._descriptionEvent = value;
  }

  get idOrga(): number {
    return this._idOrga;
  }

  set idOrga(value: number) {
    this._idOrga = value;
  }

  get dateDebEvent(): Date {
    return this._dateDebEvent;
  }

  set dateDebEvent(value: Date) {
    this._dateDebEvent = value;
  }

  get dateFinEvent(): Date {
    return this._dateFinEvent;
  }

  set dateFinEvent(value: Date) {
    this._dateFinEvent = value;
  }
}
