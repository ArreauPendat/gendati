import {PageDto} from "../page-user/DTO/page-dto";

export declare type PageList = Page[];

export class Page {
  get userToken(): string {
    return this._userToken;
  }

  set userToken(value: string) {
    this._userToken = value;
  }

  private _id:number;
  private _nom:string;
  private _type:string;
  private _lienSite:string;
  private _lienTwitter:string;
  private _lienFacebook:string;
  private _description:string;
  private _userToken:string;


  constructor() {

  }

  toPageDTO():PageDto{
    return{
      id: this._id,
      nom: this._nom,
      type: this._type,
      lienSite: this._lienSite,
      lienTwitter: this._lienTwitter,
      lienFacebook: this._lienFacebook,
      description: this._description,
      userToken: this._userToken
    };
  }

  fromPageDTO(dto: PageDto):Page{
    Object.assign(this,dto);
    return this;
  }

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get nom(): string {
    return this._nom;
  }

  set nom(value: string) {
    this._nom = value;
  }

  get type(): string {
    return this._type;
  }

  set type(value: string) {
    this._type = value;
  }

  get lienSite(): string {
    return this._lienSite;
  }

  set lienSite(value: string) {
    this._lienSite = value;
  }

  get lienTwitter(): string {
    return this._lienTwitter;
  }

  set lienTwitter(value: string) {
    this._lienTwitter = value;
  }

  get lienFacebook(): string {
    return this._lienFacebook;
  }

  set lienFacebook(value: string) {
    this._lienFacebook = value;
  }

  get description(): string {
    return this._description;
  }

  set description(value: string) {
    this._description = value;
  }

  equals(obj:Object):boolean{
    if(obj instanceof Page){
      return this._id === (<Page>obj)._id;
    }
  }
}
