import {ModereDTO} from "../page-user/DTO/modere-dto";

export class Modere {
  get idUtil(): number {
    return this._idUtil;
  }

  set idUtil(value: number) {
    this._idUtil = value;
  }

  get idPage(): number {
    return this._idPage;
  }

  set idPage(value: number) {
    this._idPage = value;
  }

  get estAdmin(): boolean {
    return this._estAdmin;
  }

  set estAdmin(value: boolean) {
    this._estAdmin = value;
  }

  private _idUtil : number;
  private _idPage : number;
  private _estAdmin : boolean;

  Modere(idUtil:number, idPage:number, estAdmin:boolean) {
    this.idUtil = idUtil;
    this.idPage = idPage;
    this.estAdmin = estAdmin;
  }

  toModereDTO() : ModereDTO {
    return {
      estAdmin: this._estAdmin,
      idPage: this.idPage,
      idUtil: this.idUtil
    }
  }

}
