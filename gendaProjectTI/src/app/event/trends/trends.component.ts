import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {EvenementService} from '../Service/evenement.service';
import {Event, EventList} from '../../model/Event';

@Component({
  selector: 'app-trends',
  templateUrl: './trends.component.html',
  styleUrls: ['./trends.component.css']
})
export class TrendsComponent implements OnInit {

  top : any[] = [
    "🥇",
    "🥈",
    "🥉",
  ]

  constructor(public eventService : EvenementService) { }

  @Output()
  reloadEvents:EventEmitter<Event> = new EventEmitter<Event>();

  emitReloadEvents(){
    this.reloadEvents.next();
  }

  ngOnInit() {
  }

  notifyEventSelected(event: Event) {
    this.eventService.notifyEventSelected(event);
  }

  private _events:EventList = [];

  get events(): Event[] {
    return this._events;
  }

  @Input()
  set events(value: Event[]) {
    this._events = value;
  }

}
