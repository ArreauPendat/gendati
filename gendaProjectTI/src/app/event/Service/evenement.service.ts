import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {EvenementDTO} from '../DTO/EvenementDTO';
import {Observable, ReplaySubject} from 'rxjs';
import {Event} from '../../model/Event';
import {LocalStorageManager} from "../../utility/local-storage-manager";
import {PageDto} from "../../page-user/DTO/page-dto";


const URL_API = "/api/evenement";
const URL_API_PARTICIPE = "/api/participe";
@Injectable({
  providedIn: 'root'
})
export class EvenementService {

  constructor(public http:HttpClient) { }

  private readonly  eventSelected:ReplaySubject<EvenementDTO> = new ReplaySubject();
  public $eventSelected : Observable<EvenementDTO> = this.eventSelected.asObservable();

  post(evenement: EvenementDTO):Observable<EvenementDTO>{
    return this.http.post<Event>(URL_API, evenement);
  }

  query():Observable<EvenementDTO[]>{
    return this.http.get<EvenementDTO[]>(URL_API);
  }

  queryEventsFromUser() : Observable<EvenementDTO[]> {
    return this.http.get<EvenementDTO[]>(URL_API + "/2&" + LocalStorageManager.getToken());
  }

  queryEventsExceptUser() : Observable<EvenementDTO[]> {
    return this.http.get<EvenementDTO[]>(URL_API + "/1&" + LocalStorageManager.getToken());
  }

  queryEventsModeration() : Observable<EvenementDTO[]> {
    return this.http.get<EvenementDTO[]>(URL_API + "/3&" + LocalStorageManager.getToken());
  }

  queryEventsPage(id:number) : Observable<EvenementDTO[]> {
    return this.http.get<EvenementDTO[]>(URL_API +"/"+ id);
  }

  get(id:number):Observable<Event>{
    return this.http.get<Event>(URL_API + "/" +id);
  }

  delete(id:number):Observable<any>{
    return this.http.delete(URL_API + '/' + id);
  }

  put(event: EvenementDTO):Observable<any> {
    return this.http.put(URL_API, event);
  }

  notifyEventSelected(event : EvenementDTO) : void {
    this.eventSelected.next(event);
  }

  joinEvent(event : EvenementDTO) : Observable<EvenementDTO> {
    return this.http.post<EvenementDTO>(URL_API_PARTICIPE, event);
  }

  leaveEvent(event : EvenementDTO) : any {
    return this.http.delete(URL_API_PARTICIPE + "/" + event.idEvent + "&" + LocalStorageManager.getToken());
  }
}
