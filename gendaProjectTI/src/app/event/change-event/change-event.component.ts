import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Event, EventList} from '../../model/Event';
import {Page} from "../../model/page";

@Component({
  selector: 'app-change-event',
  templateUrl: './change-event.component.html',
  styleUrls: ['./change-event.component.css']
})
export class ChangeEventComponent implements OnInit {

/*  readonly LABEL_DEFAULTCLASS: string = "h6 m-1";
  readonly BIGLABEL_DEFAULTCLASS:string = "h5 m-1";
  readonly INPUT_DEFAULTCLASS:string = "form-control m-2 inpWidth";*/
  readonly BUTTON_DEFAULTCLASS_CHANGE: string = "btn btn-primary m-0 btnWidth";
  readonly BUTTON_DEFAULTCLASS_DELETE: string = "btn btn-primary m-0 btnWidth"

  readonly FORMGROUP_DEFAULTCLASS: string = "form-group";
  readonly ROW_DEFAULTCLASS:string = "row d-flex";

  readonly FIELD_NOMEVENT:string = "nomEvent";
  readonly FIELD_MINEVENT:string = "nbPersMinEvent";
  readonly FIELD_MAXEVENT:string = "nbPersMaxEvent";
  readonly FIELD_DESCRIPTION:string = "descriptionEvent";
  readonly FIELD_DEBEVENT:string = "dateDebEvent";
  readonly FIELD_FINEVENT:string = "dateFinEvent";

  public tmpEvent:Event;
  public eventIsSelected:boolean = false;
  private _events:EventList = [];

  @Output()
  reloadEvents:EventEmitter<Event> = new EventEmitter<Event>();

  emitReloadEvents(){
    this.reloadEvents.next();
  }

  form:FormGroup = this.fb.group({
    nomEvent:this.fb.control("",Validators.required),
    nbPersMinEvent:this.fb.control("",Validators.required),
    nbPersMaxEvent:this.fb.control("",Validators.required),
    descriptionEvent:this.fb.control("",Validators.required),
    dateDebEvent:this.fb.control("",Validators.required),
    dateFinEvent:this.fb.control("",Validators.required)
  });
  @Output()
  eventSelected:EventEmitter<Event> = new EventEmitter<Event>();
  @Output()
  eventSelectedForDelete:EventEmitter<Event> = new EventEmitter<Event>();


  constructor(public fb:FormBuilder) {
  }

  ngOnInit() {

  }

  get events(): Event[] {
    return this._events;
  }
  @Input()
  set events(value: Event[]) {
    this._events = value;
  }

  emitSelectedEvent(e: Event) {
    this.tmpEvent = e;
    this.eventIsSelected = true;
    console.log(this.form);
    this.form.get("nomEvent").setValue(e.nomEvent);
    this.form.get("nbPersMinEvent").setValue(e.nbPersMinEvent);
    this.form.get("nbPersMaxEvent").setValue(e.nbPersMaxEvent);
    this.form.get("descriptionEvent").setValue(e.descriptionEvent);
    this.form.get("dateDebEvent").setValue(e.dateDebEvent);
    this.form.get("dateFinEvent").setValue(e.dateFinEvent);
    this.form.get(this.FIELD_NOMEVENT).disable();
    this.form.get(this.FIELD_MINEVENT).disable();
    this.form.get(this.FIELD_MAXEVENT).disable();
    this.form.get(this.FIELD_DESCRIPTION).disable();
    this.form.get(this.FIELD_DEBEVENT).disable();
    this.form.get(this.FIELD_FINEVENT).disable();
  }

  emitChangeEvenement(){
    this.tmpEvent.nomEvent = this.form.get("nomEvent").value;
    this.tmpEvent.nbPersMinEvent = this.form.get("nbPersMinEvent").value;
    this.tmpEvent.nbPersMaxEvent = this.form.get("nbPersMaxEvent").value;
    this.tmpEvent.descriptionEvent = this.form.get("descriptionEvent").value;
    this.tmpEvent.dateDebEvent = this.form.get("dateDebEvent").value;
    this.tmpEvent.dateFinEvent = this.form.get("dateFinEvent").value;
    this.eventSelected.next(this.tmpEvent);
  }

  emitDeleteEvenement() {
    this.tmpEvent.nomEvent = this.form.get("nomEvent").value;
    this.tmpEvent.nbPersMinEvent = this.form.get("nbPersMinEvent").value;
    this.tmpEvent.nbPersMaxEvent = this.form.get("nbPersMaxEvent").value;
    this.tmpEvent.descriptionEvent = this.form.get("descriptionEvent").value;
    this.tmpEvent.dateDebEvent = this.form.get("dateDebEvent").value;
    this.tmpEvent.dateFinEvent = this.form.get("dateFinEvent").value;
    this.eventSelectedForDelete.next(this.tmpEvent);
    this.form.reset();
    this.eventIsSelected = false;
  }

  public isValid():boolean{
    const tmpEvent = new Event()
    tmpEvent.nomEvent = this.form.get("nomEvent").value;
    tmpEvent.nbPersMinEvent = this.form.get("nbPersMinEvent").value;
    tmpEvent.nbPersMaxEvent = this.form.get("nbPersMaxEvent").value;
    tmpEvent.descriptionEvent = this.form.get("descriptionEvent").value;
    tmpEvent.dateDebEvent = this.form.get("dateDebEvent").value;
    tmpEvent.dateFinEvent = this.form.get("dateFinEvent").value;
    tmpEvent.dateDebEvent = new Date(tmpEvent.dateDebEvent);



    if(tmpEvent.dateDebEvent < new Date())
    {
      return false;
    }
    if(tmpEvent.nbPersMaxEvent < tmpEvent.nbPersMinEvent)
    {
      return false;
    }
    if(tmpEvent.nbPersMinEvent > 3000 || tmpEvent.nbPersMaxEvent > 3000)
    {
      return false;
    }
    return true;
  }

  activateField(field: string) {
    this.form.get(field).enable();
  }
}
