import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventMyListSmartComponent } from './event-my-list-smart.component';

describe('EventMyListSmartComponent', () => {
  let component: EventMyListSmartComponent;
  let fixture: ComponentFixture<EventMyListSmartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventMyListSmartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventMyListSmartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
