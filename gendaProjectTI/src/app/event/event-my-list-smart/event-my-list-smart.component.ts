import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {Subscription} from "rxjs";
import {Event, EventList} from "../../model/Event";
import {EvenementService} from "../Service/evenement.service";
import {PageDto} from "../../page-user/DTO/page-dto";
import {EvenementDTO} from "../DTO/EvenementDTO";

@Component({
  selector: 'app-event-my-list-smart',
  templateUrl: './event-my-list-smart.component.html',
  styleUrls: ['./event-my-list-smart.component.css']
})
export class EventMyListSmartComponent implements OnInit,OnDestroy {

  private subscriptions: Subscription[] = [];
  private events: EventList = [];

  constructor(public eventService : EvenementService) { }

  ngOnInit() {
    this.loadEvents();
  }

  ngOnDestroy(): void {
    for (let i = this.subscriptions.length - 1; i >= 0; i--) {
      const subscription = this.subscriptions[i];
      subscription && subscription.unsubscribe();
      this.subscriptions.pop();
    }
  }

  loadEvents(){
    this.events.splice(0, this.events.length);
    const sub = this.eventService
      .queryEventsFromUser()
      .subscribe(events => {
        this.events = events.map(events => new Event().fromEvenementDTO(events))
      });
    this.subscriptions.push(sub);
  }

  leaveEvent($event: EvenementDTO) {
    this.subscriptions.push(
      this.eventService
        .leaveEvent($event)
        .subscribe(
          () => {
            this.events.splice(0, this.events.length);
            this.loadEvents();
          }
        )
    )
  }

}
