import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventMyListComponent } from './event-my-list.component';

describe('EventMyListComponent', () => {
  let component: EventMyListComponent;
  let fixture: ComponentFixture<EventMyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventMyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventMyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
