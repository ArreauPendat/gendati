import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {EvenementService} from "../Service/evenement.service";
import {Event, EventList} from "../../model/Event";
import {EvenementDTO} from "../DTO/EvenementDTO";

@Component({
  selector: 'app-event-my-list',
  templateUrl: './event-my-list.component.html',
  styleUrls: ['./event-my-list.component.css']
})
export class EventMyListComponent implements OnInit {

  constructor(public eventService : EvenementService) { }

  @Output()
  private leaveEvent:EventEmitter<EvenementDTO> = new EventEmitter<EvenementDTO>();

  @Output()
  reloadEvents:EventEmitter<Event> = new EventEmitter<Event>();

  emitReloadEvents(){
    this.reloadEvents.next();
  }

  nameSearched:string;

  sortByWhat: string = "asc";

  ngOnInit() {
  }

  notifyEventSelected(event: Event) {
    this.eventService.notifyEventSelected(event);
  }

  private _events:EventList = [];

  get events(): Event[] {
    return this._events;
  }

  @Input()
  set events(value: Event[]) {
    this._events = value;
  }

  notifyLeaveEvent(i: number) {
    this.leaveEvent.next(this._events[i]);
    this._events.splice(i, 1);
  }
}
