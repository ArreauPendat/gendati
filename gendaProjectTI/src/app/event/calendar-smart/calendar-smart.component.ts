import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {Event, EventList} from '../../model/Event';
import {EvenementService} from '../Service/evenement.service';

@Component({
  selector: 'app-calendar-smart',
  templateUrl: './calendar-smart.component.html',
  styleUrls: ['./calendar-smart.component.css']
})
export class CalendarSmartComponent implements OnInit, OnDestroy {

  private subscriptions: Subscription[] = [];
  private events: EventList = [];

  constructor(public evenementService : EvenementService) { }

  ngOnInit() {
    this.loadEvent();
  }

  ngOnDestroy(): void {
    for (let i = this.subscriptions.length - 1; i >= 0; i--) {
      const subscription = this.subscriptions[i];
      subscription && subscription.unsubscribe();
      this.subscriptions.pop();
    }
  }

  loadEvent(){
    const sub = this.evenementService
      .queryEventsFromUser()
      .subscribe(events => {
        this.events = events.map(events => new Event().fromEvenementDTO(events))
      });
    this.subscriptions.push(sub);
  }



}
