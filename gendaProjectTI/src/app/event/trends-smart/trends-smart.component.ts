import { Component, OnInit } from '@angular/core';
import {Subscription} from 'rxjs';
import {Event, EventList} from '../../model/Event';
import {EvenementService} from '../Service/evenement.service';

@Component({
  selector: 'app-trends-smart',
  templateUrl: './trends-smart.component.html',
  styleUrls: ['./trends-smart.component.css']
})
export class TrendsSmartComponent implements OnInit {

  private subscriptions: Subscription[] = [];
  private events: EventList = [];

  constructor(public eventService : EvenementService) { }

  ngOnInit() {
    this.loadEvents();
  }

  ngOnDestroy(): void {
    for (let i = this.subscriptions.length - 1; i >= 0; i--) {
      const subscription = this.subscriptions[i];
      subscription && subscription.unsubscribe();
      this.subscriptions.pop();
    }
  }

  loadEvents(){
    const sub = this.eventService
      .query()
      .subscribe(events => {
        this.events = events.map(events => new Event().fromEvenementDTO(events))
      });
    this.subscriptions.push(sub);
  }

}
