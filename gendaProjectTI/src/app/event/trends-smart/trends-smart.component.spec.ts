import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrendsSmartComponent } from './trends-smart.component';

describe('TrendsSmartComponent', () => {
  let component: TrendsSmartComponent;
  let fixture: ComponentFixture<TrendsSmartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrendsSmartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrendsSmartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
