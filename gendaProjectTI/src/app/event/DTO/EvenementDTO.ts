import {Time} from "@angular/common";

export interface EvenementDTO {
  idEvent:number;
  nomEvent:string;
  nbPersMinEvent:number;
  nbPersMaxEvent:number;
  descriptionEvent:string;
  idOrga:number;
  dateDebEvent:Date;
  dateFinEvent:Date;
  userToken?:string;
}
