import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {EvenementDTO} from "../DTO/EvenementDTO";
import {EvenementService} from "../Service/evenement.service";
import {PageDto} from "../../page-user/DTO/page-dto";
import {LocalStorageManager} from "../../utility/local-storage-manager";

@Component({
  selector: 'app-event-display-smart',
  templateUrl: './event-display-smart.component.html',
  styleUrls: ['./event-display-smart.component.css']
})
export class EventDisplaySmartComponent implements OnInit, OnDestroy {

  private subscriptions: Subscription[] = [];
  private eventSelected: EvenementDTO;

  constructor(public eventService : EvenementService) { }

  ngOnInit() {
    this.listenToEventSelected();
  }

  ngOnDestroy(): void {
    for (let i = this.subscriptions.length - 1; i >= 0; i--) {
      const subscription = this.subscriptions[i];
      subscription && subscription.unsubscribe();
      this.subscriptions.pop();
    }
  }

  listenToEventSelected() {
    this.subscriptions.push(
      this.eventService
        .$eventSelected
        .subscribe(event => this.eventSelected = event)
    );
  }

}
