import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventDisplaySmartComponent } from './event-display-smart.component';

describe('EventDisplaySmartComponent', () => {
  let component: EventDisplaySmartComponent;
  let fixture: ComponentFixture<EventDisplaySmartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventDisplaySmartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventDisplaySmartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
