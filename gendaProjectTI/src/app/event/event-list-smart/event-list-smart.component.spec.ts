import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventListSmartComponent } from './event-list-smart.component';

describe('EventListSmartComponent', () => {
  let component: EventListSmartComponent;
  let fixture: ComponentFixture<EventListSmartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventListSmartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventListSmartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
