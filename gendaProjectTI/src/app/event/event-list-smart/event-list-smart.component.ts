import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {Page, PageList} from "../../model/page";
import {Event, EventList} from "../../model/Event";
import {EvenementService} from "../Service/evenement.service";
import {EvenementDTO} from "../DTO/EvenementDTO";
import {LocalStorageManager} from "../../utility/local-storage-manager";

@Component({
  selector: 'app-event-list-smart',
  templateUrl: './event-list-smart.component.html',
  styleUrls: ['./event-list-smart.component.css']
})
export class EventListSmartComponent implements OnInit, OnDestroy {

  private subscriptions: Subscription[] = [];
  private events: EventList = [];

  constructor(public eventService : EvenementService) { }

  ngOnInit() {
    this.loadEvents();
  }

  ngOnDestroy(): void {
    for (let i = this.subscriptions.length - 1; i >= 0; i--) {
      const subscription = this.subscriptions[i];
      subscription && subscription.unsubscribe();
      this.subscriptions.pop();
    }
  }

  joinThisEvent($event : EvenementDTO) {
    $event.userToken = LocalStorageManager.getToken();
    this.subscriptions.push(
      this.eventService.joinEvent($event).subscribe()
    );
    this.loadEvents();
    this.loadEvents();
  }

  // Remettre queryEventsExceptUser() quand la requête sera correcte
  loadEvents(){
    this.events.splice(0, this.events.length);
    const sub = this.eventService
      .queryEventsExceptUser()
      .subscribe(events => {
        this.events = events.map(events => new Event().fromEvenementDTO(events))
      });
    this.subscriptions.push(sub);
  }
}
