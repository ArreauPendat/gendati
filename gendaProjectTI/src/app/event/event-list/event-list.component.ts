import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {PageService} from "../../page-user/Services/page.service";
import {EvenementService} from "../Service/evenement.service";
import {Event, EventList} from "../../model/Event";
import {Page, PageList} from "../../model/page";
import {EvenementDTO} from "../DTO/EvenementDTO";

@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.css']
})
export class EventListComponent implements OnInit {

  constructor(public eventService : EvenementService) { }

  nameSearched:string;

  sortByWhat: string = "asc";

  @Output()
  reloadEvents:EventEmitter<Event> = new EventEmitter<Event>();

  @Output()
  private joinThisEvent:EventEmitter<EvenementDTO> = new EventEmitter();

  emitReloadEvents(){
    this.reloadEvents.next();
  }

  newJoinEvent(event:Event) {
    this.joinThisEvent.next(event.toEvenementDTO());
  }

  ngOnInit() {
  }

  notifyEventSelected(event: Event) {
    this.eventService.notifyEventSelected(event);
  }

  private _events:EventList = [];

  get events(): Event[] {
    return this._events;
  }

  @Input()
  set events(value: Event[]) {
    this._events = value;
  }
}
