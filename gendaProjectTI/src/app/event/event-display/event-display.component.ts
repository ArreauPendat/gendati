import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Event} from "../../model/Event";
import {EvenementDTO} from "../DTO/EvenementDTO";

@Component({
  selector: 'app-event-display',
  templateUrl: './event-display.component.html',
  styleUrls: ['./event-display.component.css']
})
export class EventDisplayComponent implements OnInit {

  private _event:Event;

  get event(): Event {
    return this._event;
  }

  @Input()
  set event(value: Event) {
    this._event = value;
  }

  constructor() { }

  ngOnInit() {
  }

}
