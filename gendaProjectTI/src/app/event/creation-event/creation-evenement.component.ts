import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Event} from '../../model/Event';
import {CalendarComponent} from '../calendar/calendar.component';
import DateTimeFormat = Intl.DateTimeFormat;
import {Page} from "../../model/page";
import {Router} from "@angular/router";

@Component({
  selector: 'app-creation-evenement',
  templateUrl: './creation-evenement.component.html',
  styleUrls: ['./creation-evenement.component.css']
})
export class CreationEvenementComponent implements OnInit {

  private _page:Page;

  get page(): Page {
    return this._page;
  }

  @Input()
  set page(value: Page) {
    this._page = value;
  }

  readonly LABEL_DEFAULTCLASS: string = "h6 m-1";
  readonly BIGLABEL_DEFAULTCLASS:string = "h5 m-1";
  readonly INPUT_DEFAULTCLASS:string = "form-control m-2 inpWidth";
  readonly BUTTON_DEFAULTCLASS: string = "btn btn-primary m-0 btnWidth";
  readonly FORMGROUP_DEFAULTCLASS: string = "form-group";
  readonly ROW_DEFAULTCLASS:string = "row d-flex";


  constructor(public fb:FormBuilder,public router:Router) { }

  form:FormGroup = this.fb.group({
    nomEvent:this.fb.control("",Validators.required),
    nbPersMinEvent:this.fb.control("",Validators.required),
    nbPersMaxEvent:this.fb.control("",Validators.required),
    descriptionEvent:this.fb.control("",Validators.required),
    dateDebEvent:this.fb.control("",Validators.required),
    dateFinEvent:this.fb.control("",Validators.required)
  })

  @Output()
  evenementCreated:EventEmitter<Event> = new EventEmitter<Event>();

  ngOnInit() {
  }

  emitNewEvenement() {
    this.evenementCreated.next(this.buildEvenement());
    //this.form.reset();
  }

  redirectAfterCreation(){
    this.router.navigate([("/event/management")]);
  }

  private buildEvenement():Event{
    const evenement = new Event();
    evenement.nomEvent = this.form.get("nomEvent").value;
    evenement.nbPersMinEvent = this.form.get("nbPersMinEvent").value;
    evenement.nbPersMaxEvent = this.form.get("nbPersMaxEvent").value;
    evenement.descriptionEvent = this.form.get("descriptionEvent").value;
    evenement.idOrga = this.page.id;
    evenement.dateDebEvent = this.form.get("dateDebEvent").value;
    evenement.dateFinEvent = this.form.get("dateFinEvent").value;
    console.log(evenement);

    return evenement;
  }

  public isValid():boolean{
    const tmpEvent = new Event()
    tmpEvent.nomEvent = this.form.get("nomEvent").value;
    tmpEvent.nbPersMinEvent = this.form.get("nbPersMinEvent").value;
    tmpEvent.nbPersMaxEvent = this.form.get("nbPersMaxEvent").value;
    tmpEvent.descriptionEvent = this.form.get("descriptionEvent").value;
    tmpEvent.dateDebEvent = this.form.get("dateDebEvent").value;
    tmpEvent.dateFinEvent = this.form.get("dateFinEvent").value;
    tmpEvent.dateDebEvent = new Date(tmpEvent.dateDebEvent);



    if(tmpEvent.dateDebEvent < new Date())
    {
      return false;
    }
    if(tmpEvent.nbPersMaxEvent < tmpEvent.nbPersMinEvent)
    {
      return false;
    }
    if(tmpEvent.nbPersMinEvent > 3000 || tmpEvent.nbPersMaxEvent > 3000)
    {
      return false;
    }
    return true;
  }
}


//TODO "All pages" doit renvoyer toutes les pages du site (sauf celles que l'utilisateur suit déjà/a le droit de modération)
//TODO "My events" doit renvoyer tous les évènements des pages de l'utilisateur, même celles où il a le droit d'administrer
