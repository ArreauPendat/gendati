import {Component, Input, OnChanges, OnInit} from '@angular/core';
import { EventSettingsModel,View } from '@syncfusion/ej2-schedule/src';
import {Event, EventList} from '../../model/Event';
import {formatI18nPlaceholderName} from '@angular/compiler/src/render3/view/i18n/util';
import DateTimeFormat = Intl.DateTimeFormat;
import {log} from 'util';
import {environment} from '../../../environments/environment';
import {Environment} from '@angular/compiler-cli/src/ngtsc/typecheck/src/environment';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit, OnChanges {

  private _events:EventList = [];
  private _calendarReference = {};
  public  esm: EventSettingsModel;

  constructor() { }

  ngOnInit() {
   this.esm = {
      dataSource:
        []
    }
  }

  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
    this.importEventsIntoCalendar();
  }

  get events(): Event[] {
    return this._events;
  }

  @Input()
  set events(value: Event[]) {
    value.map(val => val.dateDebEvent = new Date(val.dateDebEvent));
    value.map(val => val.dateFinEvent = new Date(val.dateFinEvent));
    value.map(val => console.log(val.dateDebEvent,val.dateFinEvent));
    console.log(value);
    this._events = value;
  }


  title = 'Angular-Calendar';

  importEventsIntoCalendar(){

    for (let i = 0; i < this._events.length; i++) {
      console.log(this._events[i])
      this.esm.dataSource[i] =
      {
        Id: this._events[i].idEvent,
        Subject: this._events[i].nomEvent,
        StartTime: this._events[i].dateDebEvent,
        EndTime: this._events[i].dateFinEvent,
        Description: this._events[i].descriptionEvent +" | Nombre de participant : "+this._events[i].nbPersMinEvent + " à " +this._events[i].nbPersMaxEvent,
      };
      console.log(this.esm.dataSource[i]);
    }
    console.log(this.esm);
    this.eventSettings = this.esm;

  }
  public readonly : boolean = true;
  public scheduleViews : View[] = ['Week'];
  public selectedDate: Date = new Date(Date.now());
  public eventSettings: EventSettingsModel;
}
