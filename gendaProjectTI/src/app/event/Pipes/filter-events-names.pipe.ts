import { Pipe, PipeTransform } from '@angular/core';
import {Event} from "../../model/Event";

@Pipe({
  name: 'filterEventsNames'
})
export class FilterEventsNamesPipe implements PipeTransform {

  transform(strings: Event[], filter: string): Event[] {
    if(!filter)
    {
      return strings;
    }
    const filterLowered = filter.toLowerCase();
    return strings.filter(
      value => value.nomEvent.toLowerCase().includes(filterLowered)
    );
  }

}
