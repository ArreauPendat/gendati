import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreationEventSmartComponent } from './creation-event-smart.component';

describe('CreationEventSmartComponent', () => {
  let component: CreationEventSmartComponent;
  let fixture: ComponentFixture<CreationEventSmartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreationEventSmartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreationEventSmartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
