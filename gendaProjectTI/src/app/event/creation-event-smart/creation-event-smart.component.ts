import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {EvenementService} from '../Service/evenement.service';
import {Event} from '../../model/Event';
import {PageService} from "../../page-user/Services/page.service";
import {PageDto} from "../../page-user/DTO/page-dto";

@Component({
  selector: 'app-creation-event-smart',
  templateUrl: './creation-event-smart.component.html',
  styleUrls: ['./creation-event-smart.component.css']
})
export class CreationEventSmartComponent implements OnInit, OnDestroy {

  private subscriptions: Subscription[] = [];
  private pageSelected: PageDto;

  constructor(public evenementService : EvenementService, public pageService : PageService) { }

  ngOnInit() {
    this.listenToPageSelected();
    console.log(this.pageSelected);
  }

  ngOnDestroy(): void {
    for (let i = this.subscriptions.length - 1; i >= 0; i--) {
      const subscription = this.subscriptions[i];
      subscription && subscription.unsubscribe();
      this.subscriptions.pop();
    }
  }

  createEvenement($event: Event) {
    console.log("createEvenement");
    const sub = this.evenementService.post($event.toEvenementDTO()).subscribe();
    this.subscriptions.push(sub);
  }

  listenToPageSelected() {
    this.subscriptions.push(
      this.pageService
        .$pageSelected
        .subscribe(page => this.pageSelected = page)
    );
  }

}
