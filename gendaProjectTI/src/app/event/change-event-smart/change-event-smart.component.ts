import {Component, OnDestroy, OnInit} from '@angular/core';
import {EvenementService} from '../Service/evenement.service';
import {Subscription} from 'rxjs';
import {Event, EventList} from '../../model/Event';

@Component({
  selector: 'app-change-event-smart',
  templateUrl: './change-event-smart.component.html',
  styleUrls: ['./change-event-smart.component.css']
})
export class ChangeEventSmartComponent implements OnInit, OnDestroy {

  private subscriptions: Subscription[] = [];
  private events: EventList = [];
  private eventSelectedForDelete : Event;

  constructor(public evenementService : EvenementService) { }

  ngOnInit() {
    this.loadEvent();
  }

  ngOnDestroy(): void {
    for (let i = this.subscriptions.length - 1; i >= 0; i--) {
      const subscription = this.subscriptions[i];
      subscription && subscription.unsubscribe();
      this.subscriptions.pop();
    }
  }

  loadEvent(){
    const sub = this.evenementService
      .queryEventsModeration()
      .subscribe(events => {
        this.events = events.map(events => new Event().fromEvenementDTO(events))
      });
    this.subscriptions.push(sub);
  }

  changeEvenement($event: Event) {
    console.log("change Evenement");
    const sub = this.evenementService.put($event.toEvenementDTO()).subscribe();
    this.subscriptions.push(sub);
  }

  deleteEvent($event:Event) {
    console.log("Delete Event");
    const sub = this.evenementService
      .delete($event.idEvent)
      .subscribe(events => this.deleteReferencesofEventOf($event)
      );
    this.subscriptions.push(sub);
  }

  private deleteReferencesofEventOf(event:Event){
    const indexEventFound = this.events.map(e => e.idEvent).indexOf(event.idEvent);
    if(indexEventFound != -1){
      this.events.splice(indexEventFound,1);
    }
  }
}
