import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeEventSmartComponent } from './change-event-smart.component';

describe('ChangeEventSmartComponent', () => {
  let component: ChangeEventSmartComponent;
  let fixture: ComponentFixture<ChangeEventSmartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeEventSmartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeEventSmartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
