import { Component, OnInit } from '@angular/core';
import {LocalStorageManager} from "../../utility/local-storage-manager";
import {Router} from "@angular/router";
import {UtilisateurConnectedService} from "../../user/Services/utilisateur-connected.service";
import {Local} from "protractor/built/driverProviders";

@Component({
  selector: 'app-navigation-bar',
  templateUrl: './navigation-bar.component.html',
  styleUrls: ['./navigation-bar.component.css']
})
export class NavigationBarComponent implements OnInit {

  userToken:string;

  constructor(public router:Router) { }

  ngOnInit() {
  }

  logOut() {
    LocalStorageManager.resetToken();
    window.location.reload();
  }

  isConnected(){
    this.userToken = LocalStorageManager.getToken();
    return this.userToken!=null;
  }
}
