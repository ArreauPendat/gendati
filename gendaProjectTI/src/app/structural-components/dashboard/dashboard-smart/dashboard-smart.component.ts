import {Component, Input, OnInit, Output} from '@angular/core';
import {LocalStorageManager} from "../../../utility/local-storage-manager";
import {Router} from "@angular/router";
import {RoutingValues} from "../../../utility/routing-values";

@Component({
  selector: 'app-dashboard-smart',
  templateUrl: './dashboard-smart.component.html',
  styleUrls: ['./dashboard-smart.component.css']
})
export class DashboardSmartComponent implements OnInit {

  readonly DASHBOARD_BUTTON_HIDE: string = "btn btn-danger mt-1";
  readonly DASHBOARD_BUTTON_SHOW: string = "btn btn-success mt-1";
  readonly  DEFAULT_LABEL:string = "row justify-content-center h3 mt-5 mb-1";
  readonly DEFAULT_SPAN:string = "ml-3";
  readonly DEFAULT_DIV:string = "pt-1 pb-1 border-top border-bottom cursor title";

  /* Selected value on dashboard */
  ALL_PAGES:string="inactive "+this.DEFAULT_DIV;
  ALL_EVENTS:string="inactive "+this.DEFAULT_DIV;
  TRENDS:string="inactive "+this.DEFAULT_DIV;

  MYPAGES:string="inactive "+this.DEFAULT_DIV;
  MYEVENTS:string="inactive "+this.DEFAULT_DIV;
  MYGENDA:string="inactive "+this.DEFAULT_DIV;

  MMYEVENTS:string="inactive "+this.DEFAULT_DIV;
  MMYPAGES:string="inactive "+this.DEFAULT_DIV;
  NEWPAGE:string="inactive "+this.DEFAULT_DIV;

  tabClass:string[] = [this.ALL_PAGES,this.ALL_EVENTS,this.TRENDS,this.MYPAGES,this.MYEVENTS,this.MYGENDA,this.MMYPAGES,this.MMYEVENTS,this.NEWPAGE];

  setSelected(selected:number){
    for (let i = 0; i < this.tabClass.length ; i++) {
      this.tabClass[i] = "inactive "+this.DEFAULT_DIV;
    }
    this.tabClass[selected]="active "+this.DEFAULT_DIV;
  }
  /* End of selected value on dashboard */

  isHidden:boolean=true;

  constructor(public router:Router) { }

  ngOnInit() {
  }

  // GLOBAL
  goToAllPages(){
    this.routing(RoutingValues.ALL_PAGES_ROUTING);

  }

  goToAllEvents(){
    this.routing(RoutingValues.ALL_EVENTS_ROUTING);
  }

  goToTrends(){
    this.routing(RoutingValues.TRENDS_ROUTING);
  }

  // FEED
  goToMyPages(){
    this.routing(RoutingValues.MY_PAGES_ROUTING);
  }

  goToMyEvents(){
    this.routing(RoutingValues.MY_EVENTS_ROUTING);
  }

  goToMyGenda(){
    this.routing(RoutingValues.MY_GENDA_ROUTING);
  }

  // MANAGEMENT
  goToManagementMyPages(){
    this.routing(RoutingValues.MANAGEMENT_MY_PAGES_ROUTING);
  }

  goToManagementMyEvents(){
    this.routing(RoutingValues.MANAGEMENT_MY_EVENTS_ROUTING);
  }

  goToManagementNewPage(){
    this.routing(RoutingValues.MANAGEMENT_NEW_PAGE_ROUTING);
  }

  private routing(route: string) {
    this.router.navigate([(route)]);
  }


}
