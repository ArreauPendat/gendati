import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.css']
})
export class FaqComponent implements OnInit {
  readonly LABEL_STYLE: string = "h2 text-info mt-5 mb-3";

  constructor() { }

  ngOnInit() {
  }

}
