import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FourNulFourErrorComponent } from './four-nul-four-error.component';

describe('FourNulFourErrorComponent', () => {
  let component: FourNulFourErrorComponent;
  let fixture: ComponentFixture<FourNulFourErrorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FourNulFourErrorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FourNulFourErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
